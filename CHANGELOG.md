## v.0.9.4

 * Upgade botframework to most recent version
 * Move 'known headers' to config file
 * Columns for agent reservation in ScoreSheet configurable in config
 * Enable/Disable the whole puzzle-game in the config
 * Bugfix: Before-Start Interval not working

## v0.9.3

* Include sbt-assembly for generation of fat-jars
* I18N - Translation to english, Scenario for language choosing
* Write Tg UserId to Scoresheet too
* Let the bot reply after a not understood message (ie. if no active Scenario and no command was given)
* Let the bot give feedback about requirements after uploading stats
* Refactor Callback Handling, Inline Keyboard Handling and pimped Stages to handle Message only, Callback only or both

## v0.9.2

* Periodically reload the entries in the Puzzle Sheet to check for already solved items
* Refactor Sheets Type Classes
* Bugfix: When two users first contact happened at (nearly) the same time, it was possible that they got assigned the same line in the Scoresheet
* Feature: Make constraints for Stats-Upload adjustable in Config file. Possible choices are:
  - ENFORCE_START_INTERVAL: If true, an upload of StartStats will be rejected if startstats_interval lies in the past
  - ENFORCE_END_INTERVAL: If true, an upload of EndStats will be rejected if not in endstats_interval
  - ALLOW_OVERWRITE_ENDSTATS: Whether or not its possible to upload EndStats multiple times