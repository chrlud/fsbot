package actors

import java.util.Date

import actors.NotificationsActor.NotifyMeAt
import akka.actor.{Actor, Props}
import org.joda.time.DateTime
import slogging.LazyLogging

import com.github.nscala_time.time.Imports._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

class NotificationsActor extends Actor with LazyLogging {
  logger.trace("NotificationsActor")

  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  override def receive: Receive = {
    case NotifyMeAt(time, msgToSend) =>
      logger.trace(s"NotifyMeAt($time, $msgToSend)")
      val when = new FiniteDuration(time.getMillis - DateTime.now.getMillis, MILLISECONDS)
      logger.debug(s"Calculated Delay is: $when")
      val capturedSender = sender()
      context.system.scheduler.scheduleOnce(when){
        logger.debug(s"Send scheduled Message: $msgToSend to $capturedSender")
        capturedSender ! msgToSend
      }
  }
}

object NotificationsActor {
  def props = Props(new NotificationsActor)

  case class NotifyMeAt(time: DateTime, msgToSend: Any)
}
