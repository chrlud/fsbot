package actors

import java.io.File
import java.nio.file.Paths

import scala.collection.mutable
import scala.util.Random

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import com.bot4s.telegram.models.InputFile
import config.ConfigService
import config.TimeoutConfig._
import slogging.LazyLogging

import FindPortalGame._
import scala.util.Try
import scala.concurrent.Await
import gsheets.SheetsReader
import gsheets.data.PuzzleEntry
import gsheets.data.SingleRange
import gsheets.SheetsReaderInstances._
import actors.PuzzleStatusChecker.CheckStates
import scala.concurrent.ExecutionContext
import actors.PuzzleStatusChecker.SolvedOnServer
import scala.concurrent.duration._
import gsheets.data.MultiPuzzleRange
import scala.util.Success
import scala.util.Failure
import gsheets.data.PuzzleIndex

class FindPortalGame(refreshRate: Int) extends Actor with LazyLogging {
  implicit val dispatcher = context.dispatcher
  import PuzzleState._

  val puzzleChecker = context.actorOf(PuzzleStatusChecker.props)

  val puzzleDirectory = new File(ConfigService.runtimeConfig.get.puzzleDirectory)

  val puzzleStates = mutable.Map.empty[PuzzleIndex, PuzzleState]
  val puzzleImages = mutable.Map.empty[PuzzleIndex, InputFile]
  var initialized = false

  override def receive: Receive = {
    case Init =>
      initialized match {
        case true => // already initialized - nothing to do
        case false => 
          logger.trace("Init")
          initPuzzleGame
          initialized = true
          initPuzzleChecker
      }
    case _ if !initialized => sender() ! NotInitialized
    case DrawPuzzleQuestion =>
      logger.trace("DrawPuzzleQuestion")
      drawPuzzleQuestion match {
        case Right(question)  => 
          sender ! question
          puzzleStates.update(question.puzzleIndex, INPROCESS)
        case Left(otherReply) => sender ! otherReply
      }
    case QueryPuzzleQuestion(index) =>
      logger.trace(s"QueryPuzzleQuestion($index)")
      queryPuzzleQuestion(index) match {
        case Right(question)  => sender ! question
        case Left(otherReply) => sender ! otherReply
      }
    case Unlock(index) => 
      logger.trace(s"Unlock($index)")
      if(puzzleStates(index) == INPROCESS) puzzleStates.update(index, OPEN)
    case SolvePuzzle(index) => 
      logger.trace(s"SolvePuzzle($index)")
      if(puzzleStates(index) == INPROCESS) puzzleStates.update(index, SOLVED)
    case QueryProgress =>
      logger.trace(s"QueryProgress")
      val n = puzzleStates.size
      val solved = puzzleStates.values.count(_ == SOLVED)
      val inprocess = puzzleStates.values.count(_ == INPROCESS)
      sender() ! PuzzleProgress(n, solved, inprocess)
    case StartCheckOpenPuzzle => 
      logger.trace("StartCheckOpenPuzzle")
      val openPuzzles = puzzleStates.filter(_._2 == OPEN).map(_._1).toList
      puzzleChecker ! CheckStates(openPuzzles)
    case SolvedOnServer(ix) => 
      logger.trace(s"SolvedOnServer($ix)")
      puzzleStates.update(ix, SOLVED)
  }

  def initPuzzleGame: Unit = {
    puzzleDirectory.listFiles.map{ imgFile =>
      val filename = imgFile.getName
      val puzzleIndex = PuzzleIndex.parse(filename.takeWhile(_ != '.'))
      val fileref = InputFile.Path(Paths.get(imgFile.toURI))
      (puzzleIndex, fileref)
    } foreach {
      case (None, _) => // mach was...
      case (Some(index), fileref) =>
        puzzleStates += ((index, OPEN))
        puzzleImages += ((index, fileref))
    }
  }

  def initPuzzleChecker: Unit = {
    context.system.scheduler.schedule(Duration.Zero, refreshRate.seconds, context.self, StartCheckOpenPuzzle)
  }

  private def drawPuzzleQuestion: Either[FindPortalGameReply, PuzzleQuestion] = {
    val openIndices = puzzleStates.filter(_._2 == OPEN).map(_._1).toList
    if(openIndices.isEmpty){
      Left(NoOpenPuzzle)
    } else {
      val randomIndex = pickRandomElement(openIndices)
      Right(PuzzleQuestion(randomIndex, puzzleStates(randomIndex), puzzleImages(randomIndex)))
    }
  }

  private def queryPuzzleQuestion(index: PuzzleIndex): Either[FindPortalGameReply, PuzzleQuestion] = {
    puzzleStates(index) match {
        case OPEN      =>
          puzzleStates.update(index, INPROCESS)
          Right(PuzzleQuestion(index, puzzleStates(index), puzzleImages(index)))
        case SOLVED    => Left(AlreadySolved)
        case INPROCESS => Left(BeingProgressed)
      }
  }
}

object FindPortalGame {
  def props(refreshRate: Int) = Props(new FindPortalGame(refreshRate))

  object PuzzleState extends Enumeration {
    type PuzzleState = Value
    val OPEN, SOLVED, INPROCESS = Value
  }
  import PuzzleState._

  trait FindPortalGameQuery
  case object Init extends FindPortalGameQuery
  case object DrawPuzzleQuestion extends FindPortalGameQuery
  case class QueryPuzzleQuestion(puzzleIndex: PuzzleIndex) extends FindPortalGameQuery
  case class Unlock(puzzleIndex: PuzzleIndex) extends FindPortalGameQuery
  case class SolvePuzzle(puzzleIndex: PuzzleIndex) extends FindPortalGameQuery
  case object QueryProgress extends FindPortalGameQuery
  case object StartCheckOpenPuzzle extends FindPortalGameQuery

  trait FindPortalGameReply
  case object AlreadySolved extends FindPortalGameReply
  case object BeingProgressed extends FindPortalGameReply
  case object NoOpenPuzzle extends FindPortalGameReply
  case object NotInitialized extends FindPortalGameReply
  case class PuzzleQuestion(puzzleIndex: PuzzleIndex, puzzleState: PuzzleState, image: InputFile) extends FindPortalGameReply
  case class PuzzleProgress(n: Int, solved: Int, inprocess: Int) extends FindPortalGameReply

  val r = new Random(123)
  def pickRandomElement[T](elements: List[T]): T = elements(r.nextInt(elements.length))
  def randomize[T](l: List[T]): List[T] = r.shuffle(l)

}

class PuzzleStatusChecker extends Actor  with LazyLogging {
  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  override def receive: Actor.Receive = {
    case CheckStates(indices) => 
      Try{ Await.result(SheetsReader.read(MultiPuzzleRange(indices)), timeout.duration) } match {
        case Success(Some(bulkPuzzleEntry)) => 
          bulkPuzzleEntry.entries.foreach{
            case (pin, pentry) => 
              logger.trace(s"SolvedOnServer($pin)")
              sender() ! SolvedOnServer(pin)
          }
        case _ => // ??
      }
  }

}

object PuzzleStatusChecker {
  def props = Props(new PuzzleStatusChecker)

  case class CheckStates(indices: List[PuzzleIndex])
  case class SolvedOnServer(index: PuzzleIndex)
}