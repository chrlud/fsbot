package actors

import actors.BotActor.{Activate, Deactivate, SwitchFlushing}
import actors.BotActor.Reply
import actors.BotActor.WithReply
import actors.PortalLookup.Lookup
import actors.PortalLookup.LookupResult
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import akka.pattern.pipe
import cats.instances.future._
import cats.syntax.functor._
import com.bot4s.telegram.api.AkkaTelegramBot
import com.bot4s.telegram.api.RequestHandler
import com.bot4s.telegram.clients.AkkaHttpClient
import com.bot4s.telegram.future.Polling
import com.bot4s.telegram.methods.AnswerInlineQuery
import com.bot4s.telegram.methods.EditMessageText
import com.bot4s.telegram.methods.GetFile
import com.bot4s.telegram.methods.Request
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.methods.SendPhoto
import com.bot4s.telegram.models.CallbackQuery
import com.bot4s.telegram.models.ChosenInlineResult
import com.bot4s.telegram.models.InlineQuery
import com.bot4s.telegram.models.InlineQueryResultArticle
import com.bot4s.telegram.models.InlineQueryResultLocation
import com.bot4s.telegram.models.InputFile
import com.bot4s.telegram.models.InputFile.Path
import com.bot4s.telegram.models.InputTextMessageContent
import com.bot4s.telegram.models.Message
import config.TimeoutConfig._
import slogging.LazyLogging

import java.nio.file.Paths
import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success
import config.ConfigService

class BotInterface(token: String) extends AkkaTelegramBot with Polling {
  logger.trace(s"BotInterface($token)")

  override val client: RequestHandler[Future] = new AkkaHttpClient(token)
  var botActor: ActorRef = _

  override def receiveMessage(msg: Message): Future[Unit] = {
    botActor ! msg
    super.receiveMessage(msg)
  }

  override def receiveInlineQuery(inlineQuery: InlineQuery): Future[Unit] = {
    botActor ! inlineQuery
    super.receiveInlineQuery(inlineQuery)
  }

  override def receiveCallbackQuery(callbackQuery: CallbackQuery): Future[Unit] = {
    botActor ! callbackQuery
    super.receiveCallbackQuery(callbackQuery)
  }

  def run(botActor: ActorRef): Future[Unit] = {
    this.botActor = botActor
    super.run()
  }

  override def shutdown: Unit = {
    super.shutdown()
    Await.result(system.terminate(), 2.minutes)
  }
}

class BotActor(botTarget: BotInterface) extends Actor with LazyLogging{
  logger.debug(s"BotActor: ${context.self}")

  override def preStart(): Unit = {
    context.become(flushing(false), false)
    val delay = ConfigService.delayBeforeStart.get.seconds
    logger.info(s"Bot activation delayed for $delay")
    context.system.scheduler.scheduleOnce(delay){
      logger.debug("Activating Bot Actor")
      context.self ! Activate
    }
  }
  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  lazy val splitUsers = ServiceRegistry.lookup[ActorRef]("SplitUsers").get 
  lazy val portalLookup = ServiceRegistry.lookup[ActorRef]("PortalLookup").get

  val adminId = ConfigService.adminTgId.get

  override def receive: Receive = {
    case Deactivate | SwitchFlushing => context.become(flushing(true), false)
    case BotActor.IsFlushing => sender ! BotActor.FlushingState(false)
    case msg: Message =>
      logger.debug(s"Incoming Message: $msg")
      splitUsers ! msg
    case inline: InlineQuery =>
      logger.debug(s"Received InlineQuery: $inline")
      val lookupResult = (portalLookup ? Lookup(inline.query)).mapTo[LookupResult]
      lookupResult.onComplete {
        case Success(lookupResult) =>
          val queryResults = lookupResult.portalInfos.map{portalInfo =>
            InlineQueryResultArticle(portalInfo.id, portalInfo.name, InputTextMessageContent(s"${portalInfo.name}\n${portalInfo.intelUrl}"))
          }
          botTarget.request(AnswerInlineQuery(inline.id, queryResults))
        case Failure(exception) =>
          logger.warn(s"Exception during InlineQuery: ${exception.getMessage}")
          exception.printStackTrace
      }
    case cb: CallbackQuery =>
      logger.debug(s"Received Callback: $cb")
      splitUsers ! cb
    case msg: SendMessage =>
      logger.debug(s"Send Message to User: $msg")
      botTarget.request(msg)
    case photo: SendPhoto =>
      logger.debug(s"Send Photo to User: $photo")
      botTarget.request(photo)
    case getFile: GetFile =>
      botTarget.request(getFile).pipeTo(sender())
    case r: Request[_] =>
      logger.debug(s"Send Request: $r")
      botTarget.request(r).onComplete {
        case Success(value) =>
          logger.debug(s"SUCCESS: $value")
        case Failure(exception) =>
          logger.debug(s"FAILURE: $exception")
          exception.printStackTrace
      }
    case WithReply(req) =>
      botTarget.request(req).map(Reply(_)).pipeTo(sender())
  }

  def flushing(admin: Boolean): Receive = {
    case msg: Message if(admin && msg.chat.id == adminId) => 
      logger.debug(s"Incoming Admin Message: $msg\n(Admin Passtrough)")
      splitUsers ! msg
    case cb: CallbackQuery if(admin && cb.from.id == adminId) =>
      logger.debug(s"Received Callback: $cb")
      splitUsers ! cb
    case Activate | SwitchFlushing =>
      context.unbecome
    case BotActor.IsFlushing => sender ! BotActor.FlushingState(true)
    case r: Request[_] if(admin) =>
      logger.debug(s"Send Request: $r")
      botTarget.request(r).onComplete {
        case Success(value) =>
          logger.debug(s"SUCCESS: $value")
        case Failure(exception) =>
          logger.debug(s"FAILURE: $exception")
          exception.printStackTrace
      }
    case WithReply(req) if(admin) =>
      botTarget.request(req).map(Reply(_)).pipeTo(sender())
    case anything => 
      logger.warn(s"Flushing message:\n$anything")
  }
}

object BotActor {
  def props(botTarget: BotInterface) = Props(new BotActor(botTarget))

  case class WithReply[T](req: Request[T])
  case class Reply[T](r: T)
  object Activate
  object Deactivate
  object SwitchFlushing
  object IsFlushing
  case class FlushingState(isFlushing: Boolean)
}
