package actors

import actors.NotificationsActor.NotifyMeAt
import actors.SplitUsers.{BroadcastChat, Accounts}
import akka.actor.{Actor, ActorRef, Props}
import config.ConfigService
import slogging.LazyLogging

import scala.collection.{mutable, immutable}
import scala.concurrent.Await
import com.bot4s.telegram.models.Message
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models.CallbackQuery

class SplitUsers extends Actor with LazyLogging {
  logger.trace("SplitUsers()")

  lazy val botActor = ServiceRegistry.lookup[ActorRef]("BotActor").get

  val notificationsActor = context.actorOf(NotificationsActor.props)
  initNotifications(notificationsActor)

  val accounts = mutable.Map[Long, ActorRef]()

  override def receive: Receive = {
    case msg: Message => getOrCreateAccount(msg) ! msg
    case cb: CallbackQuery => 
      accounts.get(cb.from.id) match {
        case Some(acc) => acc ! cb
        case None => logger.warn("Discard CallbackQuery because there is no account")
      }
    case BroadcastChat(txt) => {
      logger.debug(s"Receive BroadcastMessage($txt) send to ${accounts.keys} with $botActor")
      for {
        chatId <- accounts.keys
      } yield botActor ! SendMessage(chatId, txt)
    }
    case Accounts =>
      sender ! immutable.Map(accounts.toSeq: _*)
  }

  private def getOrCreateAccount(msg: Message): ActorRef = {
    if(!accounts.contains(msg.chat.id)){
      val newAccountant = for {
        user <- msg.from
        _ <- {logger.debug(s"Create account Actor for new user ${user.username.getOrElse(user.firstName)}"); Some()}
        actorId <- user.username.orElse(Some(user.id.toString))
      } yield context.actorOf(AccountantActor.props(user, msg.chat), name = s"account_$actorId")

      newAccountant match {
        case Some(ref) =>
          accounts += (msg.chat.id -> ref)
          logger.debug(s"User account created: $msg.user -> $ref")
        case None =>
          logger.warn(s"Received Message with no 'from': $msg")
      }
    }
    accounts(msg.chat.id)
  }

  private def initNotifications(notificationsActor: ActorRef): Unit = {
    val eventConfig = ConfigService.eventConfig.get
    Map(
      eventConfig.startstats_interval.getStart -> "Die Registrierung ist geöffnet. Stats jetzt hochladen (/upload).",
//      eventConfig.event_interval.getStart -> "Das Event beginnt.",
      eventConfig.event_interval.getEnd -> "Das Event ist beendet. Bitte Endstats hochladen. (/upload)",
      eventConfig.endstats_interval.getEnd -> "Die Annahme der Endstats ist geschlossen. Das Event ist jetzt beendet"
    ).filter{ case (timestamp, _) => timestamp.isAfterNow} foreach {
      case (timestamp, txt) => notificationsActor ! NotifyMeAt(timestamp, BroadcastChat(txt))
    }
  }
}

object SplitUsers {
  def props = Props(new SplitUsers)

  case class BroadcastChat(txt: String)
  case object Accounts
}