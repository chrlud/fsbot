package actors

import actors.PortalLookup.{Lookup, LookupResult, hashSearch}
import akka.actor.{Actor, Props}
import config.ConfigService
import slogging.LazyLogging

import scala.io.Source

class PortalLookup extends Actor with LazyLogging {
  val portalInfos = Source.fromFile(ConfigService.runtimeConfig.get.portalLookupFile).getLines.map{ line =>
    val data = line.split(';')
    val portalName = data(0)
    val longitude = data(1).toDouble
    val latitude = data(2).toDouble
    val id = data(3)
    PortalInfo(portalName, longitude, latitude, id)
  }.toSeq
  logger.trace(s"PortalLookup initialzed with ${portalInfos.size} portals")

  override def receive: Receive = {
    case Lookup(searchString) =>
      logger.trace(s"Lookup($searchString)")
      val hashedSearchString = hashSearch(searchString)
      if(hashedSearchString.length > 2){
        val result = LookupResult(portalInfos.filter(_.searchHash.contains(hashedSearchString)).take(49))
        logger.trace(s"Found ${result.portalInfos.size} results")
        sender() ! result
      } else {
        logger.trace(s"Suppress PortalLookup for short search string: $hashedSearchString")
        sender() ! LookupResult(Seq())
      }
  }
}

object PortalLookup {
  def props = Props(new PortalLookup)

  case class Lookup(searchString: String)
  case class LookupResult(portalInfos: Seq[PortalInfo])

  def hashSearch(searchString: String) = searchString.replaceAll("\\s+", "").toLowerCase()
}

case class PortalInfo(name: String, lng: Double, lat: Double, id: String){
  val searchHash = hashSearch(name)
  val intelUrl = s"http://intel.ingress.com/intel?pll=$lat,$lng"
}