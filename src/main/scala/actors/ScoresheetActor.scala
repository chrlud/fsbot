package actors

import scala.concurrent.ExecutionContext

import actors.ScoresheetActor._
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import akka.pattern.pipe
import com.bot4s.telegram.models.User
import config.TimeoutConfig._
import gsheets.data.ScoreSheetEntryRange
import gsheets.data.StatsUseridRange
import gsheets.data.SheetsRange
import gsheets.SheetsReader
import gsheets.SheetsReaderInstances._
import gsheets.SheetsWriterInstances._
import gsheets.SheetsWriterSyntax._
import gsheets.data.SingleRange
import slogging.LazyLogging
import scala.concurrent.Await
import scala.util.Try
import scala.concurrent.Future
import gsheets.WriteResult
import gsheets.data.ScoreSheetLastLineRange
import gsheets.data.ScoresheetEntry
import gsheets.data.ScoreSheetReservationRange

class ScoresheetActor extends Actor with LazyLogging{

  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  override def receive: Receive = {
    case RetrieveRanges(user) =>
      logger.trace(s"RetrieveRanges($user)")

      val range = ScoreSheetReservationRange()
      SheetsReader.read(range)  // Future[Option[ScoreSheetReservations]]
        .map{_  // Option[ScoreSheetReservations]
          .flatMap{reservations => 
            reservations.findUser(user.id)
              .map(reservation => RangeResponse(user, ScoreSheetEntryRange(reservation.line)))
          }.getOrElse(NotFound)
        }.pipeTo(sender())

    case RegisterNewLine(user) =>
      logger.trace(s"RegisterNewLine($user)")
      val lastLineRange = ScoreSheetLastLineRange()
      val rangeResponse: Future[RangeResponse] = for {
        lastLineResult <- SheetsReader.read(lastLineRange)
        insertLine <- Future.successful(lastLineResult.map(_.n + 1).getOrElse(lastLineRange.firstLine))
        writeResult <- user.writeTo(StatsUseridRange(insertLine))
      } yield RangeResponse(user, ScoreSheetEntryRange(insertLine))

      Await.ready(rangeResponse, timeout.duration)
      rangeResponse.pipeTo(sender())

  }
}

object ScoresheetActor {
  def props = Props(new ScoresheetActor)

  trait ScoreSheetCommand
  trait ScoreSheetResponse

  final case class RegisterOrRetrieveLine(user: User) extends ScoreSheetCommand
  final case class RetrieveRanges(user: User) extends ScoreSheetCommand
  final case class RegisterNewLine(user: User) extends ScoreSheetCommand
  final case class RangeResponse(user: User, range: ScoreSheetEntryRange) extends ScoreSheetResponse
  final object NotFound extends ScoreSheetResponse
}
