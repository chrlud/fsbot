package actors

import akka.actor.Actor

import scala.collection.mutable.Map
import akka.actor.Props
import ServiceRegistry.Register
import ServiceRegistry.Lookup
import ServiceRegistry.LookupResult
import ServiceRegistry.LookupFailed
import ServiceRegistry.LookupSuccess
import akka.actor.ActorRef
import akka.pattern.ask
import config.TimeoutConfig._
import scala.concurrent.Await
import akka.actor.ActorSystem
import slogging.LazyLogging

class ServiceRegistry extends Actor with LazyLogging {

    val services = Map.empty[String, Any]

    override def receive: Receive = {
        case Register(name, service) => 
            logger.trace(s"Registered: ('$name', $service)")
            services += ((name, service))
        case Lookup(name) => 
            logger.trace(s"Lookup('$name')")
            services.get(name) match {
                case Some(service) => 
                    logger.trace(s"Successful Lookup ('$name', $service)")
                    sender() ! LookupSuccess(name, service)
                case None => 
                    logger.warn(s"Lookup of '$name' failed!")
                    sender() ! LookupFailed(name)
            }
    }
}

object ServiceRegistry {
    private def props = Props(new ServiceRegistry)

    private sealed case class Register(name: String, service: Any)
    private sealed case class Lookup(name: String)
    private trait LookupResult
    private  case class LookupSuccess(name: String, service: Any) extends LookupResult
    private  case class LookupFailed(name: String) extends LookupResult

    private var globalServiceRegistry: ActorRef = _

    def init(system: ActorSystem) = 
        globalServiceRegistry = system.actorOf(ServiceRegistry.props, "ServiceRegistry")

    def register(name: String, service: Any): Unit = globalServiceRegistry ! Register(name, service)

    import scala.reflect.ClassTag
    def lookup[T: ClassTag](name: String): Option[T] = {
        val lookupResult = Await.result(globalServiceRegistry.ask(Lookup(name)).mapTo[LookupResult], timeout.duration)
        lookupResult match {
        case LookupSuccess(name, service: T) => 
            Some(service)
        case LookupFailed(_) => None
        case _ => 
            None
        }
    }
}