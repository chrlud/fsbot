package actors

import actors.AccountantActor.Init
import actors.ScoresheetActor.RegisterNewLine
import actors.ScoresheetActor.RetrieveRanges
import actors.ScoresheetActor.ScoreSheetResponse
import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.Stash
import akka.pattern.ask
import com.bot4s.telegram.models.Chat
import com.bot4s.telegram.models.User
import com.osinka.i18n.Lang
import config.TimeoutConfig._
import gsheets.SheetsReader
import gsheets.SheetsReaderInstances._
import gsheets.data.MultiRange
import gsheets.data.ScoreSheetEntryRange
import gsheets.data.SingleRange
import gsheets.data.Stats
import scenarios._
import scenarios.puzzle.PuzzleScenario
import scenarios.stats.StatsScenario
import slogging.LazyLogging
import telegram.Escritoire

import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import ScoresheetActor.RangeResponse
import ScoresheetActor.NotFound
import config.ConfigService
import scenarios.admin.AdminScenario

class AccountantActor(user: User, chat: Chat) extends Actor with Stash with Commands with LazyLogging {
  logger.trace(s"AccountActor($user, $chat)")

  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  override def preStart(): Unit = {
    logger.trace("Becoming uninitialized")
    context.become(initReceive, discardOld = false)
    context.self ! Init
    super.preStart()
  }
  lazy val scoresheetService = ServiceRegistry.lookup[ActorRef]("ScoresheetService").get
  lazy val botActor = ServiceRegistry.lookup[ActorRef]("BotActor").get

  def initReceive: Receive = {
    case Init =>
      logger.trace(s"Init")
      // TODO: What if this fails say for an error on the sheets side
      val (userState, env) = Await.result(
        for {
          existingRange <- (scoresheetService ? RetrieveRanges(user)).mapTo[ScoreSheetResponse]
          _ <- logFutureMonad(s"Existing Range: $existingRange")
          rangeResponse <- existingRange match {
            case r: RangeResponse => Future.successful(r)
            case NotFound => (scoresheetService ? RegisterNewLine(user)).mapTo[RangeResponse]
          }
          _ <- logFutureMonad(s"Received Range Response: $rangeResponse")
          startStats <- SheetsReader.read(rangeResponse.range.toStartRange)
          endStats <- SheetsReader.read(rangeResponse.range.toEndRange)
          lang <- optionToFuture(user.languageCode).map(langCode => Lang(langCode))
        } yield (
          AgentState(startStats.flatMap(_.data.get("Agent Name")), Submitted(startStats.isDefined, endStats.isDefined), false, lang),
          Environment(user, chat, rangeResponse.range, new Escritoire(user, botActor))
        ), timeout.duration
      )

      this.state = userState
      this.env = env
      logger.trace(s"Init UserState: $userState")
      logger.trace(s"Init Environment: $env")

      val helpScenario = context.actorOf(HelpScenario.props)
      Map(
        "start" -> context.actorOf(StartScenario.props),
        "upload" -> context.actorOf(StatsScenario.props),
        "?" -> helpScenario,
        "help" -> helpScenario,
        "info" -> context.actorOf(StatusScenario.props),
        "selfie" -> context.actorOf(SelfieScenario.props),
        "puzzle" -> context.actorOf(PuzzleScenario.props),
        "choose" -> context.actorOf(InlineKBTest.props),
        "lang" -> context.actorOf(LangScenario.props)
      ).foreach{
        case (str, ref) =>
          registerCommandWith(str, ref)
          ref ! this.env
          ref ! this.state
      }
      if(ConfigService.adminTgId.get == user.id) {
        val adminCommand = context.actorOf(AdminScenario.props)
        registerCommandWith("admin", adminCommand)
        adminCommand ! this.env
        adminCommand ! this.state
      }

      logger.trace("Unbecoming uninitialized")
      unstashAll
      context.unbecome
    case _ => stash()
  }

  private def logFutureMonad(msg: String): Future[Unit] = Future.successful(logger.trace(msg))
  private def optionToFuture[T](o: Option[T]): Future[T] = Future{ o.get }

}

object AccountantActor {
  def props(user: User, chat: Chat) = Props(new AccountantActor(user, chat))

  case object Init

}

case class Environment(user: User,
                       chat: Chat,
                       scoresheetRange: ScoreSheetEntryRange,
                       escritoire: Escritoire)