package util

import scala.concurrent.Future
import akka.actor.ActorSystem
import akka.pattern.Patterns.after
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import com.typesafe.scalalogging.LazyLogging
import java.io.IOException
import config.TimeoutConfig

object ExpBackoffStrategy extends LazyLogging {
    final private val init = 1
    final private val factor = 1.3F
    final private val maxWait = (2.minutes).toMillis

    object BackoffTimeout extends Exception

    private def inc(wait: Int) = Math.ceil(wait * factor).toInt
    private def checkTimeout(startMillis: Long) = {
        val elapsed = System.currentTimeMillis() - startMillis
        if(elapsed <= maxWait) {
            Future.successful(1)
        }
        else {
            Future.failed(BackoffTimeout)
        }
    }
  
    def doRetries[T](fn: () => Future[T], 
                     startMillis: Long = System.currentTimeMillis(), 
                     wait: Int = init)
                     (implicit as: ActorSystem, ec: ExecutionContext): Future[T] = 
        checkTimeout(startMillis).flatMap{ _ =>
            fn().recoverWith{ 
                case ex: IOException => 
                    logger.debug(s"Retry after $wait Seconds due to Exception ${ex.getMessage()}")
                    akka.pattern.after(wait.seconds) {
                        doRetries(fn, startMillis, inc(wait))
                    }
            }
        }
}
