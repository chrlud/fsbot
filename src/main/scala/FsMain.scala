package main {

import actors.{BotInterface, BotActor, ServiceRegistry, FindPortalGame, ScoresheetActor}
import akka.actor.ActorRef
import com.typesafe.config.ConfigFactory
import config.{ConfigService, EventConfig, RuntimeConfig, ScoresheetConfig}
import gsheets.RequestFactoryConfig
import slogging.{LazyLogging, LogLevel, LoggerConfig, SLF4JLoggerFactory}

import scala.util.{Failure, Success, Try}
import config.ParserConfig

import collection.JavaConverters._
import java.util.concurrent.atomic.AtomicBoolean
import scala.concurrent.Future
import actors.SplitUsers
import actors.PortalLookup

object FsMain extends App with LazyLogging {
  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global
  private def requestShutdownAfterEnterKey = Future {
    readLine
    shutdown
  }

  private val isShuttingDown = new AtomicBoolean(false)
  def shutdown = isShuttingDown.set(true)
  def runLoop = {
    while(!isShuttingDown.get) {
      Thread.sleep(1000)
    }
    logger.info("\n\nShutting down Server...")
    bot.shutdown
  }

  /*
   ** Startup sequence
   */
  initLogging
  loadConfig
  logger.info(printBanner)

  startServices

  val botActor = ServiceRegistry.lookup[ActorRef]("BotActor").get
  val bot = ServiceRegistry.lookup[BotInterface]("BotInterface").get
  bot.run(botActor)
  logger.info(
    """
      | --> Server is Running
      | --> Press Enter to quit <--
      |
      | """.stripMargin)
  requestShutdownAfterEnterKey

  runLoop
  /* ******************************** */

  private def initLogging = {
    LoggerConfig.factory = SLF4JLoggerFactory()
    LoggerConfig.level = LogLevel.TRACE
  }

  private def loadConfig: Unit = {
    val configFilePath = "./fsbot.conf"
    logger.trace(s"Loading App Config from path '$configFilePath'")
    val botconf = ConfigFactory.parseFile(new java.io.File(configFilePath))

    Try {
      ConfigService.eventConfig = Some( EventConfig.load(botconf.getConfig("event")) )
      ConfigService.sheetsConfig = Some(
        RequestFactoryConfig(
          USER_ID = botconf.getString("sheets.USER_ID"),
          APP_NAME = botconf.getString("sheets.APP_NAME"),
          CREDENTIALS_FILE = botconf.getString("sheets.CREDENTIALS_FILE"),
          TOKENS_DIRECTORY_PATH = botconf.getString("sheets.TOKENS_DIRECTORY_PATH"),
          RECEIVER_PORT = botconf.getInt("sheets.RECEIVER_PORT")
        ))
       
      ConfigService.scoresheetConfig = Some(ScoresheetConfig(
        botconf.getString("sheets.AUTOSCORE_SHEET_ID"),
        botconf.getString("sheets.SCORESHEET_TGID_COLUMN"),
        botconf.getString("sheets.SCORESHEET_TGNAME_COLUMN")
      ))
      ConfigService.portalsucheSheetId = Some(botconf.getString("sheets.PORTALSUCHE_SHEET_ID"))
      ConfigService.telegramBotId = Some(botconf.getString("bot.TELEGRAM_BOT_ID"))
      ConfigService.delayBeforeStart = Some(botconf.getInt("bot.STARTUP_DELAY"))
      ConfigService.adminTgId = Some(botconf.getLong("bot.ADMIN_TG_ID"))
      ConfigService.resetCommand = Some(botconf.getString("bot.RESET_COMMAND"))

      ConfigService.runtimeConfig = Some(RuntimeConfig(
        botconf.getString("runtime.SELFIE_DIRECTORY"),
        botconf.getString("runtime.PORTALS_LOOKUP_FILE"),
        botconf.getBoolean("runtime.ENABLE_PUZZLE_GAME"),
        botconf.getString("runtime.PUZZLE_DIRECTORY")))
      
      ConfigService.parserConfig = Some(ParserConfig(botconf.getStringList("parser.KNOWN_HEADERS").asScala.toList))
    } match {
      case Success(_) =>
        logger.info("Loading App Config Successful")
        logger.debug(
          s"""User Configs: ${ConfigService.userConfigs}
             |Event Configs: ${ConfigService.eventConfig}
             |Sheets Configs: ${ConfigService.sheetsConfig}
             |Scoresheet Config: ${ConfigService.sheetsConfig}
             |TG Bot ID: ${ConfigService.telegramBotId}""".stripMargin)
      case Failure(exception) =>
        logger.error("Error loading App Config", exception)
        exception.printStackTrace
        System.exit(-1)
    }
  }

  private def startServices: Unit = {
    logger.info(" --> Starting Bot System")
    val bot = new BotInterface(ConfigService.telegramBotId.get)

    ServiceRegistry.init(bot.system)
    ServiceRegistry.register("BotInterface", bot)

    val scoreSheetActor = bot.system.actorOf(ScoresheetActor.props, "ScoresheetService")
    ServiceRegistry.register("ScoresheetService", scoreSheetActor)

    if(ConfigService.runtimeConfig.get.puzzleEnabled){
      val puzzleGameService = bot.system.actorOf(FindPortalGame.props(ConfigService.eventConfig.get.puzzle_refresh_rate_seconds), "PuzzleGame")
      ServiceRegistry.register("PuzzleGame", puzzleGameService)
    }

    val splitUsers = bot.system.actorOf(SplitUsers.props, "SplitUsers")
    ServiceRegistry.register("SplitUsers", splitUsers)

    val portalLookup = bot.system.actorOf(PortalLookup.props, "PortalLookup")
    ServiceRegistry.register("PortalLookup", portalLookup)

    val botActor = bot.system.actorOf(BotActor.props(bot), "BotActor")
    ServiceRegistry.register("BotActor", botActor)
  }

  private def printBanner = {
    val eventConfig = ConfigService.eventConfig.get
    val dateFmt = eventConfig.fmtDate
    val timeFmt = eventConfig.fmtTime
    s"""
       |----------------------------------------------------
       |   Telegram Bot for Ingress First Saturday Events
       |----------------------------------------------------
       |
       |=== Event Info: ===
       |${eventConfig.EVENT_NAME} ${dateFmt.print(eventConfig.event_interval.getStart)} ${timeFmt.print(eventConfig.event_interval.getStart)} - ${timeFmt.print(eventConfig.event_interval.getEnd)}
       |
       |Login From : ${timeFmt.print(eventConfig.startstats_interval.getStart)}
       |Logout Until: ${timeFmt.print(eventConfig.endstats_interval.getEnd)}
       |""".stripMargin
  }
}
}