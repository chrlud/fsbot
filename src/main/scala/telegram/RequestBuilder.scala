package telegram

import com.bot4s.telegram.models.ChatId
import com.bot4s.telegram.models.ReplyMarkup
import com.bot4s.telegram.methods.ParseMode.ParseMode
import com.bot4s.telegram.models.InputFile
import telegram.RequestBuilderInfo.HasRecipient
import telegram.RequestBuilderInfo.HasContent
import com.bot4s.telegram.methods.Request
import com.bot4s.telegram.models.Message
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.methods.SendPhoto
import com.bot4s.telegram.models.InlineKeyboardMarkup

sealed trait RequestContent
case class TextContent(text: String, parseMode: Option[ParseMode] = None, disableWebPagePreview: Option[Boolean] = None) extends RequestContent
case class PhotoContent(photo: InputFile, caption: Option[String] = None, parseMode: Option[ParseMode] = None) extends RequestContent

sealed trait RequestBuilderInfo
object RequestBuilderInfo {
    sealed trait Empty extends RequestBuilderInfo
    sealed trait HasRecipient extends RequestBuilderInfo
    sealed trait HasContent extends RequestBuilderInfo

    type IsBuildable = Empty with HasRecipient with HasContent
    type IsAddressed = Empty with HasRecipient
}

case class RequestBuilder[I <: RequestBuilderInfo](
    chatId: Option[ChatId],
    content: Option[RequestContent],
    replyMarkup: Option[ReplyMarkup]
) {
    def withRecipient(chatId: ChatId): RequestBuilder[I with HasRecipient] =
        this.copy(chatId = Some(chatId))

    def withContent(content: RequestContent): RequestBuilder[I with HasContent] =
        this.copy(content = Some(content))

    def withTextContent(text: String, parseMode: Option[ParseMode] = None, disableWebPagePreview: Option[Boolean] = None): RequestBuilder[I with HasContent] =
        this.withContent(TextContent(text, parseMode, disableWebPagePreview))

    def withPhotoContent(photo: InputFile, caption: Option[String] = None, parseMode: Option[ParseMode] = None) =
        this.withContent(PhotoContent(photo, caption, parseMode))

    def withReplyMarkup(replyMarkup: ReplyMarkup): RequestBuilder[I] =
        this.copy(replyMarkup = Some(replyMarkup))

    def build(implicit ev: I =:= RequestBuilderInfo.IsBuildable): Request[Message] = (content: @unchecked) match {
        case Some(TextContent(text, parseMode, disableWPPreview)) =>  
            SendMessage(chatId = chatId.get, text = text, parseMode = parseMode, replyMarkup = replyMarkup, disableWebPagePreview = disableWPPreview)
        case Some(PhotoContent(photo, caption, parseMode)) => 
            SendPhoto(chatId = chatId.get, photo = photo, caption = caption, parseMode = parseMode, replyMarkup = replyMarkup)
    }
}

object RequestBuilder {
    def apply() = new RequestBuilder[RequestBuilderInfo.Empty](None, None, None)
}

object Test {
    import RequestBuilderInfo._
    val b1: RequestBuilder[IsAddressed] = RequestBuilder()
        .withRecipient(1L)

    val b2: RequestBuilder[IsBuildable] = b1.withTextContent("")

    val rq = b2.build

    def foo(builder: RequestBuilder[IsAddressed]): Request[Message] = builder.withTextContent("").build

    foo(
        RequestBuilder()
            .withRecipient(1L)
            // .withTextContent("")
            .withReplyMarkup(InlineKeyboardMarkup.singleRow(Seq()))
    )
}