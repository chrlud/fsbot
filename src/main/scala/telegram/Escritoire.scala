package telegram

import akka.actor.ActorRef
import com.bot4s.telegram.methods.SendMessage
import com.bot4s.telegram.models.{ReplyMarkup, User}
import actors.BotActor.WithReply
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.Future
import actors.BotActor.Reply
import com.bot4s.telegram.models.Message
import com.bot4s.telegram.methods.Request
import scala.concurrent.ExecutionContext

class Escritoire(val recipient: User, val botActor: ActorRef) {

  def apply(txt: String, keyboard: Option[ReplyMarkup] = None): Unit =
    botActor ! SendMessage(recipient.id, txt, replyMarkup = keyboard)

  def withReply(txt: String, keyboard: Option[ReplyMarkup] = None)
    (implicit to: Timeout, ec: ExecutionContext): Future[Message] = 
    (botActor ? WithReply(SendMessage(recipient.id, txt, replyMarkup = keyboard)))
      .mapTo[Reply[Message]]
      .map(_.r)

  def apply[T](r: Request[T]): Unit = 
    botActor ! r

  def withReply[T](r: Request[T])
    (implicit to: Timeout, ec: ExecutionContext): Future[T] =
    (botActor ? WithReply(r))
      .mapTo[Reply[T]]
      .map(_.r)
}