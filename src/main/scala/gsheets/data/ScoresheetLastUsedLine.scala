package gsheets.data

final case class ScoresheetLastUsedLine(n: Int)