package gsheets.data

import slogging.LazyLogging

import scala.util.Try
import config.ConfigService
import config.ParserConfig

case class Stats(data: Map[String, String]) {

  import Stats.knownHeaders

  val sep = "   "

  def print: String = {
    val sortedHeaders = knownHeaders.filter(data.keys.toList.contains(_))
    val unknownHeaders = data.keys.filter(!knownHeaders.contains(_))
    val allHeaders = sortedHeaders ++ unknownHeaders
    s"""${allHeaders.mkString(sep)}
       |${allHeaders.map(data(_)).mkString(sep)}""".stripMargin
  }
}

object Stats extends LazyLogging {
  import scala.collection.JavaConverters._

  lazy val parserConfig: ParserConfig = ConfigService.parserConfig.get

  def knownHeaders = parserConfig.knownHeaders

  def parse(statsString: String): Try[Stats] = Try {
    val requiredHeaders = Seq(
      "Agent Name",
      "Agent Faction",
      "Lifetime AP",
      "XM Recharged"
    )

    logger.trace("Stats.parse(statsString)")
    val lines = statsString.lines.iterator.asScala.toList
    val headerLine = lines(0)
    val dataLine = lines(1)

    assert(requiredHeaders.forall(headerLine.contains(_)), "Required header fields missing.")

    val correctedHeaderResult = headerLine.split("[ ]+")
      .foldLeft((List.empty[String],"")){
        case ((l, a), b) =>
          val concat = s"$a $b".trim
          if(knownHeaders.contains(concat)) {
            (concat :: l , "")
          } else {
            (l, concat)
          }
      }
    val correctedHeader = correctedHeaderResult._1.reverse

    val correctedData = {
      val data = dataLine.split("[ ]+").toList.map(_.trim)
      if(data.take(2).mkString(" ") == "ALL TIME") {
        "ALL TIME" :: data.drop(2)
      } else {
        data
      }
    }

    if(correctedData.length == correctedHeader.length && correctedHeaderResult._2.isEmpty){
      Stats(correctedHeader.zip(correctedData).toMap)
    } else if(correctedHeader.contains("Agent Name") && correctedHeader.length <= correctedData.length) {
      val parsedData = correctedHeader.zip(correctedData.take(correctedHeader.length))
      val unparsedData = (correctedHeaderResult._2, correctedData.drop(correctedHeader.length).mkString(" "))
      Stats((parsedData :+ unparsedData).toMap)
    } else {
      println(correctedHeader.length)
      logger.debug(s"Residual header: ${correctedHeaderResult._2}")
      throw new Exception(s"Fehler: Stats konnten nicht geparsed werden. Unbekannter Header: ${correctedHeaderResult._2}")
    }
  }
}