package gsheets.data

import com.bot4s.telegram.models.User
import config.ConfigService

object StatsPosition extends Enumeration {
    type StatsPosition = Value
    val START, END = Value
}
import StatsPosition._

sealed trait SheetsRange
object SheetsRangeConstants {
    val SC_FORMRESPONSES = "'Form Responses'"
    val SC_TIME_COL = "A"
    val SC_START_COL = "D"
    val SC_END_COL = "E"
    lazy val SC_TGID_COL = ConfigService.scoresheetConfig.get.tgIdColumn
    lazy val SC_TGNAME_COL = ConfigService.scoresheetConfig.get.tgNameColumn
    val SC_LEFT_COL = SC_TIME_COL
    val SC_RIGHT_COL = SC_TGNAME_COL

}
trait SingleRange { def toRangeString: String }
trait MultiRange { def toRangeStrings: List[String] }

final case class MultiPuzzleRange(ranges: List[PuzzleIndex]) extends SheetsRange with MultiRange {
    private val rowOffset = 7
    def toRangeStrings: List[String] = ranges.map(ix => s"'Spalte ${ix.column}'!B${ix.row+rowOffset}:D${ix.row+rowOffset}")
}
final case class PuzzleRange(ix: PuzzleIndex) extends SheetsRange with SingleRange {
    private val rowOffset = 7
    def toRangeString: String = s"'Spalte ${ix.column}'!B${ix.row+rowOffset}:D${ix.row+rowOffset}"
}
final case class ScoreSheetEntryRange(line: Int) extends SheetsRange with SingleRange {
    import SheetsRangeConstants.{SC_FORMRESPONSES, SC_START_COL}

    def toRangeString: String = s"$SC_FORMRESPONSES!$SC_START_COL$line:I$line"
    def toStartRange = StatsRange(line, START)
    def toEndRange = StatsRange(line, END)
}
final case class ScoreSheetReservationRange(firstLine: Int = 3) extends SheetsRange with SingleRange {
    import SheetsRangeConstants.{SC_FORMRESPONSES, SC_LEFT_COL, SC_TGID_COL}
    def toRangeString: String = s"$SC_FORMRESPONSES!$SC_LEFT_COL$firstLine:$SC_TGID_COL"
}
final case class ScoreSheetLastLineRange(firstLine: Int = 3) extends SheetsRange with SingleRange {
    import SheetsRangeConstants.{SC_FORMRESPONSES, SC_LEFT_COL, SC_RIGHT_COL}
    def toRangeString: String = s"$SC_FORMRESPONSES!$SC_LEFT_COL$firstLine:$SC_RIGHT_COL"
}

final case class StatsRange(line: Int, position: StatsPosition) extends SheetsRange with MultiRange {
    import SheetsRangeConstants.{SC_FORMRESPONSES, SC_START_COL, SC_END_COL}
    override def toRangeStrings: List[String] = List(
        s"'Form Responses'!A$line:A$line",
        position match {
            case START => s"$SC_FORMRESPONSES!$SC_START_COL$line:$SC_START_COL$line"
            case END => s"$SC_FORMRESPONSES!$SC_END_COL$line:$SC_END_COL$line"
        }
  )
}
final case class StatsUseridRange(line: Int) extends SheetsRange with MultiRange {
    import SheetsRangeConstants.{SC_FORMRESPONSES, SC_TGID_COL, SC_TGNAME_COL}
    override def toRangeStrings: List[String] = List(
        s"$SC_FORMRESPONSES!$SC_TGID_COL$line:$SC_TGID_COL$line",
        s"$SC_FORMRESPONSES!$SC_TGNAME_COL$line:$SC_TGNAME_COL$line"
    )
}