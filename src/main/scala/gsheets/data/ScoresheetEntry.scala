package gsheets.data

import org.joda.time.DateTime

case class ScoresheetEntry(timestamp: DateTime, userId: Int, line: Int, start: Option[Stats], end: Option[Stats])