package gsheets.data

case class PuzzleEntry(portalName: String, user: String, portalLink: String)