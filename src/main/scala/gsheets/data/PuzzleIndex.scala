package gsheets.data

case class PuzzleIndex(column: Char, row: Int) {
  def display: String = s"$column$row"
}
  object PuzzleIndex {
    def parse(txt: String): Option[PuzzleIndex] = {
      if(txt.length == 2 && txt(1).isDigit) Some(PuzzleIndex(txt(0).toUpper, txt(1).toString.toInt))
      else None
    }
  }