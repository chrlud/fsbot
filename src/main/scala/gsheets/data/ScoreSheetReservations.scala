package gsheets.data

final case class ScoreSheetReservations(entries: List[ScoreSheetReservation]) {
    def findUser(tgId: Long): Option[ScoreSheetReservation] = entries.find(_.tgId == tgId)
}
final case class ScoreSheetReservation(line: Int, tgId: Long)