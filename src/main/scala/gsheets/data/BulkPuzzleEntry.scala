package gsheets.data

final case class BulkPuzzleEntry(entries: Map[PuzzleIndex, PuzzleEntry])