package gsheets

import java.text.SimpleDateFormat
import java.util.Date

import com.bot4s.telegram.models.User
import com.google.api.services.sheets.v4.model.BatchUpdateValuesRequest
import config.ConfigService
import data.Stats

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Future}

import data.PuzzleEntry
import data.StatsRange
import data.StatsUseridRange
import data.PuzzleRange

object SheetsWriterInstances {

  implicit val statsWriter: SheetsWriter[Stats, StatsRange] = {
    new SheetsWriter[Stats, StatsRange] {
      val spreadsheetId = ConfigService.scoresheetConfig.get.sheetsId
      val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)
      val df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")

      override def write(stats: Stats, range: StatsRange)(implicit ec: ExecutionContext): Future[WriteResult] = {
        val dateRange = range.toRangeStrings(0)
        val now = new Date
        val dateVr = VR.boxValue(df.format(now), dateRange)

        val valueRange = range.toRangeStrings(1)
        val valueVr = VR.boxValue(stats.print, valueRange)

        val vrs = Seq(dateVr, valueVr).asJava

        val batchUpdateRequest = new BatchUpdateValuesRequest()
          .setData(vrs)
          .setValueInputOption("RAW")

        Future {
          requestFactory.batchUpdateRequest(spreadsheetId, batchUpdateRequest).execute
        }.map(result => WriteResult(result.getResponses.asScala.map(_.getUpdatedCells.toInt).sum))
      }
    }
  }

  implicit val userIdWriter: SheetsWriter[User, StatsUseridRange] =
    new SheetsWriter[User, StatsUseridRange] {
      val spreadsheetId = ConfigService.scoresheetConfig.get.sheetsId
      val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

      override def write(user: User, range: StatsUseridRange)(implicit ec: ExecutionContext): Future[WriteResult] = {
        val idRange = range.toRangeStrings(0)
        val userid = user.id.toString
        val idVr = VR.boxValue(userid, idRange)

        val nameRange = range.toRangeStrings(1)
        val userName = user.username.getOrElse("NoName")
        val nameVr = VR.boxValue(userName, nameRange)

        val batchUpdateRequest = new BatchUpdateValuesRequest()
          .setData(Seq(idVr, nameVr).asJava)
          .setValueInputOption("RAW")
        Future {
          requestFactory.batchUpdateRequest(spreadsheetId, batchUpdateRequest).execute
        }.map(result => WriteResult(result.getResponses.asScala.map(_.getUpdatedCells.toInt).sum))
      }
    }

    implicit val puzzleEntryWriter: SheetsWriter[PuzzleEntry, PuzzleRange] =
      new SheetsWriter[PuzzleEntry, PuzzleRange] {
        val spreadsheetId = ConfigService.portalsucheSheetId.get
        val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

        def write(puzzleEntry: PuzzleEntry, range: PuzzleRange)(implicit ec: ExecutionContext): Future[WriteResult] = {
          val vr = VR.boxRow(Seq(puzzleEntry.portalName, puzzleEntry.user, puzzleEntry.portalLink), range.toRangeString)
          Future {
            requestFactory.writeRequest(spreadsheetId, vr).execute
          }.map(response => WriteResult(response.getUpdatedCells))
        }
      }
}
