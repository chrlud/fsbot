package gsheets

import scala.concurrent.{ExecutionContext, Future}
import data.SheetsRange
import main.FsMain
import util.ExpBackoffStrategy

trait SheetsReader[A, R <: SheetsRange] {
  def read(range: R)(implicit ec: ExecutionContext): Future[Option[A]]
}

object SheetsReader {
  def read[A, R <: SheetsRange](range: R)(implicit r: SheetsReader[A, R], ec: ExecutionContext): Future[Option[A]] = {
    ExpBackoffStrategy.doRetries(() => r.read(range))(FsMain.bot.system, FsMain.ec)
  }
}