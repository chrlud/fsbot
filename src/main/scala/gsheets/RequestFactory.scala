package gsheets

import java.io.{FileInputStream, InputStreamReader}

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.{GoogleAuthorizationCodeFlow, GoogleClientSecrets}
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.sheets.v4.model.{BatchUpdateValuesRequest, BatchUpdateValuesResponse, UpdateValuesResponse, ValueRange}
import com.google.api.services.sheets.v4.{Sheets, SheetsRequest, SheetsScopes}

import scala.collection.JavaConverters._
import com.google.api.client.json.gson.GsonFactory

class RequestFactory(config: RequestFactoryConfig) {

  lazy val credentials = createCredentials()
  lazy val service = createService(credentials)

  def init: Unit = service

  def readRequest(spreadsheetId: String, range: String): AbstractGoogleClientRequest[ValueRange] =
    service.spreadsheets().values().get(spreadsheetId, range)

  def batchReadRequest(spreadsheetId: String, ranges: List[String]) = 
    service.spreadsheets().values().batchGet(spreadsheetId).setRanges(ranges.asJava)

  def writeRequest(spreadsheetId: String, vr: ValueRange): SheetsRequest[UpdateValuesResponse] =
    service.spreadsheets().values().update(spreadsheetId, vr.getRange, vr)
      .setValueInputOption("RAW")

  def batchUpdateRequest(spreadsheetId: String, batchUpdate: BatchUpdateValuesRequest): SheetsRequest[BatchUpdateValuesResponse] =
    service.spreadsheets().values().batchUpdate(spreadsheetId, batchUpdate)

  private val JSON_FACTORY = GsonFactory.getDefaultInstance()
  private val HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
  private val SCOPES = List(SheetsScopes.SPREADSHEETS).asJava

  private def createCredentials(): Credential = {
    val clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(new FileInputStream(new java.io.File(config.CREDENTIALS_FILE))))
    val flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
      .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(config.TOKENS_DIRECTORY_PATH)))
      .setAccessType("offline")
      .build()
    val receiver = new LocalServerReceiver.Builder().setPort(config.RECEIVER_PORT).build()
    new AuthorizationCodeInstalledApp(flow, receiver).authorize(config.USER_ID)
  }

  private def createService(credentials: Credential): Sheets =
    new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credentials)
      .setApplicationName(config.APP_NAME)
      .build()
}

case class RequestFactoryConfig( USER_ID: String,
                                 APP_NAME: String,
                                 CREDENTIALS_FILE: String,
                                 TOKENS_DIRECTORY_PATH: String,
                                 RECEIVER_PORT: Int )