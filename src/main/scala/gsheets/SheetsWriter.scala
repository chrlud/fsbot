package gsheets

import scala.concurrent.{ExecutionContext, Future}
import data.SheetsRange

final case class WriteResult(written: Int)

trait SheetsWriter[A, R <: SheetsRange] {
  def write(value: A, range: R)(implicit ec: ExecutionContext): Future[WriteResult]
}