package gsheets

import scala.concurrent.{ExecutionContext, Future}
import data.SheetsRange
import util.ExpBackoffStrategy
import main.FsMain

object SheetsWriterSyntax {
  implicit class SheetsWriterOps[A, R <: SheetsRange](value: A) {
    def writeTo(range: R)(implicit w: SheetsWriter[A, R], ec: ExecutionContext): Future[WriteResult] = {
      ExpBackoffStrategy.doRetries(() => w.write(value, range))(FsMain.bot.system, FsMain.ec)
    }
  }
}
