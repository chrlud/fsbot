package gsheets

import scala.collection.JavaConverters._

import com.google.api.services.sheets.v4.model.ValueRange

object ValueRangeSyntax {
    implicit class ValueRangeOps(vr: ValueRange) {
        def unboxValue: Option[String] = Option(vr.getValues).map(_.get(0).get(0).toString)
        def unboxValues: Option[List[List[String]]] = Option(vr.getValues).map(_.asScala.toList.map(_.asScala.toList.map(_.toString)))
        def unboxRow: Option[List[String]] = Option(vr.getValues).map(_.asScala.toList.map(_.asScala.toList.map(_.toString)).head)
    }
}

object VR {
    def boxValue(value: String, range: String): ValueRange =
        new ValueRange()
            .setRange(range)
            .setValues(Seq(Seq(value.asInstanceOf[Object]).asJava).asJava)

    def boxRow(data: Seq[String], range: String): ValueRange =
        new ValueRange()
            .setRange(range)
            .setValues(Seq(data.map(_.asInstanceOf[Object]).asJava).asJava)
}
