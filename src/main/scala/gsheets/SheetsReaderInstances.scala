package gsheets

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Try

import config.ConfigService
import slogging.LazyLogging

import data.Stats
import data.MultiPuzzleRange
import data.ScoresheetEntry
import data.PuzzleEntry
import data.BulkPuzzleEntry
import data.ScoreSheetEntryRange
import data.SheetsRange
import data.PuzzleRange
import data.StatsRange

import ValueRangeSyntax._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import gsheets.data.ScoresheetLastUsedLine
import gsheets.data.ScoreSheetLastLineRange
import gsheets.data.ScoreSheetReservations
import gsheets.data.ScoreSheetReservationRange
import gsheets.data.ScoreSheetReservation

object SheetsReaderInstances extends LazyLogging{

  implicit val scoresheetReader: SheetsReader[ScoresheetEntry, ScoreSheetEntryRange] =
    new SheetsReader[ScoresheetEntry, ScoreSheetEntryRange] {
      val spreadsheetId = ConfigService.scoresheetConfig.get.sheetsId
      val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

      override def read(range: ScoreSheetEntryRange)(implicit ec: ExecutionContext): Future[Option[ScoresheetEntry]] = {
        Future{ requestFactory.readRequest(spreadsheetId, range.toRangeString).execute }
          .map{vr => 
            vr.unboxRow.map{data => 
              val date = data(0)
              val startstats = data(3)
              val endstats = data(4)
              val userid = data(8)
              ScoresheetEntry(DateTime.now, userid.toInt, range.line, Stats.parse(startstats).toOption, Stats.parse(endstats).toOption)
            }
          }
      }
    }

  implicit val scoresheetReservationReader: SheetsReader[ScoreSheetReservations, ScoreSheetReservationRange] =
    new SheetsReader[ScoreSheetReservations, ScoreSheetReservationRange] {
      val spreadsheetId = ConfigService.scoresheetConfig.get.sheetsId
      val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)
      
      override def read(range: ScoreSheetReservationRange)(implicit ec: ExecutionContext): Future[Option[ScoreSheetReservations]] = {
        Future{ requestFactory.readRequest(spreadsheetId, range.toRangeString).execute }
          .map{ vr => 
            vr.unboxValues.map{ lines => 
              ScoreSheetReservations(
                lines.zipWithIndex.flatMap{ case (line, ix) => 
                  if(canConvertInt(line.last)) {
                    List(ScoreSheetReservation(ix + range.firstLine, line.last.toInt))
                  } else List()
                }
              )
            }
          }
      }

      private def canConvertInt(s: String) = Try{s.toInt}.isSuccess
    }

  implicit val scoresheetLastLineReader: SheetsReader[ScoresheetLastUsedLine, ScoreSheetLastLineRange] =
    new SheetsReader[ScoresheetLastUsedLine, ScoreSheetLastLineRange] {
      val spreadsheetId = ConfigService.scoresheetConfig.get.sheetsId
      val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

      override def read(range: ScoreSheetLastLineRange)(implicit ec: ExecutionContext): Future[Option[ScoresheetLastUsedLine]] = {
        Future{ requestFactory.readRequest(spreadsheetId, range.toRangeString).execute }
          .map{vr => 
            vr.unboxValues.flatMap{lines =>
              maxOption(lines.zipWithIndex.filter{ line => 
                  logger.debug(s"line ${line._2}: ${line._1}\nIsEmpty: ${isEmpty(line._1)}")
                  !isEmpty(line._1)
              }.map(_._2 + range.firstLine).map{nel => logger.debug(s"NonEmpty Line: $nel");nel})
            }.map(ScoresheetLastUsedLine(_))
          }
      }

      def isEmpty(line: List[String]) = line.forall(_.isBlank())
      def maxOption(lineNumbers: List[Int]): Option[Int] = lineNumbers match {
        case Nil => None
        case _ => Some(lineNumbers.max)
      }
    }

  implicit val statsReader: SheetsReader[Stats, StatsRange] =
    new SheetsReader[Stats, StatsRange] {
      val spreadsheetId = ConfigService.scoresheetConfig.get.sheetsId
      val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

      override def read(range: StatsRange)(implicit ec: ExecutionContext): Future[Option[Stats]] = {
        Future { requestFactory.readRequest(spreadsheetId, range.toRangeStrings(1)).execute }
          .map{vr =>
            vr.unboxValue
              .flatMap(Stats.parse(_).toOption)
          }
      }
    }

    implicit val puzzleEntryReader: SheetsReader[PuzzleEntry, PuzzleRange] =
      new SheetsReader[PuzzleEntry, PuzzleRange] {
        val spreadsheetId = ConfigService.portalsucheSheetId.get
        val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

        override def read(range: PuzzleRange)(implicit ec: ExecutionContext): Future[Option[PuzzleEntry]] = {
          Future{ requestFactory.readRequest(spreadsheetId, range.toRangeString).execute }
            .map{ vr => 
              vr.unboxRow.flatMap{data => 
                val portalName = data.lift(0).getOrElse("")
                val user = data.lift(1).getOrElse("")
                val portalLink = data.lift(2).getOrElse("")
                if(portalName.isBlank && user.isBlank && portalLink.isBlank)
                  None
                else Some(PuzzleEntry(portalName, user, portalLink))
              }
            }
        }
      }

    implicit val bulkPuzzleEntryReader: SheetsReader[BulkPuzzleEntry, MultiPuzzleRange] =
      new SheetsReader[BulkPuzzleEntry, MultiPuzzleRange] {
        val spreadsheetId = ConfigService.portalsucheSheetId.get
        val requestFactory = new RequestFactory(ConfigService.sheetsConfig.get)

        override def read(range: MultiPuzzleRange)(implicit ec: ExecutionContext): Future[Option[BulkPuzzleEntry]] = {
          val rangeTranslation = range.toRangeStrings.zip(range.ranges).toMap
          Future{ requestFactory.batchReadRequest(spreadsheetId, range.toRangeStrings).execute }
            .map{ batchGetValuesResponse => 
              Some(BulkPuzzleEntry(batchGetValuesResponse.getValueRanges().asScala.map{ vr => 
                val range = vr.getRange()
                val pix = rangeTranslation(range)
                vr.unboxRow.flatMap{ data => 
                  val portalName = data.lift(0).getOrElse("")
                  val user = data.lift(1).getOrElse("")
                  val portalLink = data.lift(2).getOrElse("")
                  if(portalName.isBlank && user.isBlank && portalLink.isBlank)
                    None
                  else Some(PuzzleEntry(portalName, user, portalLink))
                }.map((pix, _))
              }.filter(_.isDefined).map(_.get).toMap ))
            }
        }
      }

}
