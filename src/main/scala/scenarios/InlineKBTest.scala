package scenarios

import scala.concurrent.Await
import scala.concurrent.ExecutionContext

import akka.actor.Props
import com.bot4s.telegram.models.InlineKeyboardButton
import com.bot4s.telegram.models.InlineKeyboardMarkup
import com.bot4s.telegram.models.Message
import config.TimeoutConfig._
import scenarios.Scenario.KeepStage
import scenarios.Scenario.NextStage
import scenarios.Scenario.QuitScenario
import scenarios.Scenario.QuitWithMessage
import scenarios.inlkb.InlineKeyboardOps._
import slogging.LazyLogging

class InlineKBTest extends Scenario with LazyLogging {
  implicit val ec: ExecutionContext =
    context.system.dispatchers.lookup("blocking-io-dispatcher")

  var msgWithKB: Message = _
  var n = 0

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("Init Inline Keyboard", _ => {
      env.escritoire("Pick a number")
      msgWithKB = Await.result(env.escritoire.withReply(n.toString, InlineKBTest.markup), timeout.duration)
      NextStage
    }),
    CallbackStage("Choose Number", cb => {
      implicit val esc = env.escritoire
      val dta: String = cb.data.getOrElse("")
      dta match {
        case "less" => 
          n -= 1
          msgWithKB.updateText(n.toString)
          KeepStage
        case "more" => 
          n += 1
          msgWithKB.updateText(n.toString)
          KeepStage
        case "ok" => 
          msgWithKB.removeKeyboard
          QuitWithMessage(s"Chosen: $n")
        case "" => 
          KeepStage
      }
    })
  )
}

object InlineKBTest {
  def props = Props(new InlineKBTest)

  val markup = Some(
    InlineKeyboardMarkup.singleRow(
      Seq(
        InlineKeyboardButton.callbackData("<-", "less"),
        InlineKeyboardButton.callbackData("OK", "ok"),
        InlineKeyboardButton.callbackData("->", "more")
      )
    )
  )
}