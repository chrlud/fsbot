package scenarios

import akka.actor.Props
import akka.pattern.ask
import com.bot4s.telegram.methods.GetFile
import com.bot4s.telegram.models.File
import config.ConfigService
import config.TimeoutConfig._
import scenarios.Scenario.NextStage
import scenarios.Scenario.QuitScenario
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider
import slogging.LazyLogging

import java.io
import java.net.URL
import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import sys.process._

class SelfieScenario extends Scenario with LazyLogging {
  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  val config = ConfigService

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("request selfie", precondition = checkPreconditions _, handler =  _ => {
      NextStage(i18n"SelfieScenario.sendselfie/${state.agentName.get}")
    }),
    MessageStage("progress selfie", msg => {
      val result = msg.photo match {
        case Some(photosizes) =>
          val photo = photosizes.maxBy(_.fileSize)
          logger.debug(s"Received Photo: $photo")
          val fielid = photo.fileId
          val getfile = GetFile(fielid)
          for {
            tgfile <- (env.escritoire.botActor ? getfile).mapTo[File]
            url <- constructUrl(tgfile)
            selfiefile <- constructSelfieFile(fileextension(tgfile.filePath.get))
            _ <- logTraceFuture(s"Download Selfie from '$url' to file '$selfiefile'")
          } yield url #> selfiefile !!
        case None => Future.failed(new Exception("No image"))
      }
      Await.ready(result, timeout.duration)
      val updatedState = result.value match {
        case Some(Success(_)) =>
          env.escritoire(i18n"SelfieScenario.selfiesaved")
          state.copy(selfieUploaded = true)
        case Some(Failure(exception)) =>
          env.escritoire(i18n"SelfieScenario.errorsaving/${exception.getMessage}")
          state
        case None =>
          env.escritoire(i18n"CommonDialog.unknownError")
          state
      }
      QuitScenario(updatedState)
    })
  )
  def constructSelfieFile(extension: String) = (for {
    selfieDirectory <- config.runtimeConfig.map(_.selfieDirectory).toRight(i18n"SelfieScenario.noruntimeconfig")
    agentName <- state.agentName.toRight(i18n"CommonDialog.notLoggedId")
  } yield (selfieDirectory, agentName)) match {
    case Right((selfieDir, agentName)) =>
      Future.successful(new io.File(s"$selfieDir/$agentName.$extension"))
    case Left(errMsg) =>
      Future.failed(new Exception(errMsg))
  }

  def constructUrl(file: File) = Future {
    new URL(s"https://api.telegram.org/file/bot${config.telegramBotId.get}/${file.filePath.get}")
  }

  def downloadImage(url: URL, file: java.io.File) = {
    import sys.process._
    Future {
      url #> file !!
    }.map{_ => ()}
  }

  def checkPreconditions: Either[String, Unit] = for {
    _ <- Either.cond(state.agentName.isDefined, (), i18n"CommonDialog.notLoggedId")
    _ <- Either.cond(!state.selfieUploaded, (), i18n"SelfieScenario.alreadysubmitted")
  } yield ()

  def logTraceFuture(msg: String) = Future.successful(logger.trace(msg))
  def fileextension(filename: String) = filename.substring(filename.lastIndexOf(".")+1)
}

object SelfieScenario {
  def props = Props(new SelfieScenario)
}