package scenarios.stats

import akka.actor.Props
import akka.pattern.ask
import com.bot4s.telegram.models._
import config.ConfigService
import config.EventState
import config.EventState._
import config.TimeoutConfig._
import gsheets.SheetsWriter
import gsheets.SheetsWriterInstances._
import gsheets.SheetsWriterSyntax._
import gsheets.data.MultiRange
import gsheets.data.SheetsRange
import gsheets.data.Stats
import scenarios.MessageStage
import scenarios.Scenario
import scenarios.Scenario.NextStage
import scenarios.Scenario.QuitScenario
import scenarios.Scenario.QuitWithMessage
import scenarios.ScenarioStage
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider
import scenarios.stats.StatsScenario.PossibleActions
import scenarios.stats.StatsScenario.cancelKeyboard
import scenarios.stats.StatsScenario.removeKeyboard

import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.util.Failure
import scala.util.Success
import scala.util.Try
import scenarios.AgentState
import gsheets.SheetsReader
import gsheets.SheetsReaderInstances._
import scenarios.Emoticon

class StatsScenario extends Scenario {
  logger.trace(s"StatsScenario()")

  import PossibleActions._
  import StatsScenario._
  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  private val eventConfig = ConfigService.eventConfig.get

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("Request Upload Stats", _ => {
      val scenarioReply = for {
        action <- possibleUploadActionNow
        _      <- Right(env.escritoire(uploadMessage(action, ConfigService.eventConfig.map(_.allowOverwriteEnd).getOrElse(false)), Some(cancelKeyboard)))
      } yield NextStage
      scenarioReply match {
        case Left(message) => 
          QuitWithMessage(message)
        case Right(nextStage) => 
          nextStage
      }
    }),
    MessageStage("Upload Stats Data", msg => {
      val scenarioReply = for {
        action <- possibleUploadActionNow
        txt    <- msg.text.toRight(i18n"StatsScenario.noStatsReceived")
        _      <- Either.cond(!txt.equals("Abbruch"), (), i18n"CommonDialog.canceled")
        stats  <- Stats.parse(txt).toEither.left.map(_ => i18n"StatsScenario.errorParsingStats")
        _      <- Right(env.escritoire(i18n"StatsScenario.writingToScoresheet", Some(removeKeyboard)))
        _      <- logError(Try{ Await.result(
          action match {
            case UPLOAD_START => stats.writeTo(env.scoresheetRange.toStartRange)
            case UPLOAD_END => stats.writeTo(env.scoresheetRange.toEndRange)
          }, timeout.duration) }.toEither).left.map(_ => i18n"StatsScenario.errorWritingValues")
        _      <- Right(env.escritoire(uploadSuccess(action)))
      } yield action match {
        case UPLOAD_START => QuitScenario(state.copy(agentName = stats.data.get("Agent Name"), cellsState = state.cellsState.copy(start = true)))
        case UPLOAD_END => QuitScenario(state.copy(agentName = stats.data.get("Agent Name"), cellsState = state.cellsState.copy(`end` = true)))
      }
      scenarioReply match {
        case Left(message) => 
          logger.trace(s"MessageStage(Upload Stats Data) results in Left($message)")
          QuitWithMessage(message, removeKeyboard = true)
        case Right(quitScenario) => 
          logger.trace(s"MessageStage(Upload Stats Data) results in Right($quitScenario)")
          feedbackRequirements(quitScenario.result)
          quitScenario
      }
    })
  )

  def possibleUploadActionNow(implicit scenario: Scenario): Either[String, PossibleActions] = {
    val locale = state.lang.locale
    if(eventConfig.canUploadStart(state.cellsState)) Right(UPLOAD_START)
    else if(eventConfig.isUpcoming) Left(i18n"StatsScenario.startvaluesIn/${eventConfig.durationToFmt(START, locale)}")
    else if(!state.cellsState.start && !eventConfig.canUploadStart(state.cellsState)) Left(i18n"StatsScenario.startClosed")
    else if(eventConfig.canUploadEnd(state.cellsState)) Right(UPLOAD_END)
    else if(!state.cellsState.end && !eventConfig.canUploadEnd(state.cellsState)) Left(i18n"StatsScenario.endClosed")
    else Left(i18n"StatsScenario.signedOff")
  }

  def logError[T](err: Either[Throwable, T]) = {
    if(err.isLeft) err.left.get.printStackTrace
    err
  }

  def feedbackRequirements(newState: AgentState) = {
    if(newState.cellsState.end){
      val recharged = for{
        startStats <- SheetsReader.read(env.scoresheetRange.toStartRange)
        endStats <- SheetsReader.read(env.scoresheetRange.toEndRange)
      } yield (endStats.map(_.data("XM Recharged").toInt).toList ++ startStats.map(_.data("XM Recharged").toInt).toList).reduce(_ - _)
      val selfie = state.selfieUploaded

      recharged.onComplete{
        case Success(recharged) => 
          val msg = Seq(
            if(recharged > 5000) i18n"StatsScenario.enoughrecharged/${Emoticon.GREEN_CHECK}"
            else i18n"StatsScenario.notenoughrecharged/${Emoticon.RED_CROSS}",
            if(selfie) i18n"StatsScenario.selfieuploaded/${Emoticon.GREEN_CHECK}"
            else i18n"StatsScenario.noselfieuploaded/${Emoticon.RED_CROSS}"
          ).mkString("\n")
          env.escritoire(msg)
        case Failure(exception) => // never mind
      }
    }
  }

}

object StatsScenario {
  def props: Props =
    Props(new StatsScenario)

  object PossibleActions extends Enumeration {
    type PossibleActions = Value
    val UPLOAD_START, UPLOAD_END = Value
  }
  import PossibleActions._

  def cancelKeyboard(implicit scenario: Scenario) = ReplyKeyboardMarkup.singleButton(
    KeyboardButton(i18n"CommonDialog.cancel"), oneTimeKeyboard = Some(true), resizeKeyboard = Some(true)
  )

  def removeKeyboard = ReplyKeyboardRemove()

  def uploadMessage(action: PossibleActions, allowReupload: Boolean)(implicit
        scenario: Scenario
    ): String = action match {
      case UPLOAD_START =>
        List(
          i18n"StatsScenario.uploadStart",
          i18n"StatsScenario.sendAllTimeStats",
          i18n"StatsScenario.tapCancel"
        ).mkString("\n")
      case UPLOAD_END =>
        List(
          i18n"StatsScenario.uploadEnd",
          i18n"StatsScenario.sendAllTimeStats",
          if (allowReupload) i18n"StatsScenario.reuploadOk"
          else i18n"StatsScenario.noReupload",
          i18n"StatsScenario.tapCancel"
        ).mkString("\n")
    }

    def uploadSuccess(action: PossibleActions)(implicit scenario: Scenario): String = action match {
        case UPLOAD_START => i18n"StatsScenario.startSuccess"
        case UPLOAD_END => i18n"StatsScenario.endSuccess"
    }
}

