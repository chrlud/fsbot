package scenarios

import akka.actor.Props
import com.github.nscala_time.time.Imports.DateTimeFormat
import config.ConfigService
import scenarios.Scenario._
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider
import slogging.LazyLogging

class StartScenario extends Scenario with LazyLogging {
  logger.trace(s"StartScenario")

  val dateformat = DateTimeFormat.forPattern("dd.MM.yyyy")
  val timeformat = DateTimeFormat.forPattern("HH:mm")
  def welcome = ConfigService.eventConfig.map{config =>
    Seq(
      i18n"StartScenario.welcometo/${config.EVENT_NAME}",
      i18n"StartScenario.date/${dateformat.print(config.event_interval.getStart)}",
      i18n"StartScenario.duration/${timeformat.print(config.event_interval.getStart)},${timeformat.print(config.event_interval.getEnd)}",
      i18n"StartScenario.starttime/${timeformat.print(config.startstats_interval.getStart)}",
      i18n"StartScenario.endtime/${timeformat.print(config.endstats_interval.getEnd)}",
      "",
      i18n"StartScenario.help"
    ).mkString("\n")
  }.getOrElse(i18n"StartScenario.wrongconfig")

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("Send Welcome", _ => {
      env.escritoire(welcome)
      QuitScenario(state)
    })
  )
}

object StartScenario {
  def props = Props(new StartScenario)
}