package scenarios

import akka.actor.Props
import com.bot4s.telegram.models.User

class KnockKnockScenario extends Scenario {
  import Scenario._

  var n: Int = 1

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("Frage Stage", _ => {
      env.escritoire("Knock Knock!\nWho's there?")
      NextStage
    }),
    MessageStage("Nachfrage Stage", msg => {
      env.escritoire(msg.text.getOrElse("Weihnachtsmann") + " who?")
      NextStage
    }),
    MessageStage("ROFL Stage", _ => {
      env.escritoire("Haha, good one!")
      env.escritoire(s"This was joke number ${n}")
      n += 1
      QuitScenario(state)
    })
  )
}

object KnockKnockScenario {
  def props(implicit user: User) = Props(new KnockKnockScenario())
}