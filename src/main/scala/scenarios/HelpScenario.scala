package scenarios

import akka.actor.Props
import scenarios.Scenario.QuitWithMessage
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider

class HelpScenario extends Scenario {

  def messageText = List(
    i18n"HelpScenario.infocmd",
    i18n"HelpScenario.uploadcmd",
    i18n"HelpScenario.selfiecmd",
    i18n"HelpScenario.langcmd",
    i18n"HelpScenario.puzzlecmd",
    i18n"HelpScenario.inlinecmd",
    i18n"HelpScenario.helpcmd"
  ).mkString("\n\n")

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("", _ => {
      QuitWithMessage(messageText)
    })
  )
}

object HelpScenario {
  def props = Props(new HelpScenario)
}