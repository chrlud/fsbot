package scenarios.i18n

import scenarios.Scenario

object LocaleProviderInstances {

    implicit val scenarioLocaleProvider = new LocaleProvider[Scenario] {
        def locale(scenario: Scenario) = scenario.state.lang
    }
  
}
