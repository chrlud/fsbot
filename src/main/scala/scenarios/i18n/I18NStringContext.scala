package scenarios.i18n

import com.osinka.i18n.Lang
import com.osinka.i18n.Messages

trait LocaleProvider[T] {
    def locale(t: T): Lang
}

object I18NStringContext{
    implicit class I18NStringContext(val sc: StringContext) {
        def i18n[T](args: Any*)(implicit localeProvider: LocaleProvider[T], t: T): String = {
            val key = sc.parts.headOption.map(_.takeWhile(_ != '/')).getOrElse("")
            Messages(key, args: _*)(localeProvider.locale(t))
        }
    }
}
