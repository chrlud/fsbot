package scenarios.puzzle

import actors.FindPortalGame
import actors.FindPortalGame._
import actors.PortalLookup
import actors.ServiceRegistry
import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import com.bot4s.telegram.methods.DeleteMessage
import com.bot4s.telegram.methods.Request
import com.bot4s.telegram.methods.SendPhoto
import com.bot4s.telegram.models.InlineKeyboardButton
import com.bot4s.telegram.models.InlineKeyboardMarkup
import com.bot4s.telegram.models.InputFile
import com.bot4s.telegram.models.KeyboardButton
import com.bot4s.telegram.models.Message
import com.bot4s.telegram.models.ReplyKeyboardMarkup
import com.bot4s.telegram.models.ReplyKeyboardRemove
import config.TimeoutConfig._
import gsheets.SheetsReader
import gsheets.SheetsReaderInstances._
import gsheets.SheetsWriterInstances._
import gsheets.SheetsWriterSyntax._
import gsheets.data.PuzzleEntry
import gsheets.data.PuzzleIndex
import gsheets.data.PuzzleRange
import gsheets.data.SingleRange
import scenarios.MessageOrCallbackStage
import scenarios.MessageStage
import scenarios.Scenario
import scenarios.Scenario.KeepStage
import scenarios.Scenario.NamedStage
import scenarios.Scenario.NextStage
import scenarios.Scenario.QuitWithError
import scenarios.Scenario.QuitWithMessage
import scenarios.Scenario.ScenarioMessage
import scenarios.ScenarioStage
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider
import scenarios.inlkb.InlineKeyboardOps._
import slogging.LazyLogging
import telegram.PhotoContent
import telegram.RequestBuilder

import scala.concurrent.Await
import scala.util.Try

class PuzzleScenario extends Scenario with LazyLogging {
  implicit val dispatcher = context.dispatcher

  lazy val puzzleService = ServiceRegistry.lookup[ActorRef]("PuzzleGame")

  var activeQuestion: Option[PuzzleQuestion] = None
  var msgWithKeyboard: Message = _
  var chosenQuestion: Option[PuzzleQuestion] = None
  var chosenSolution: Option[PuzzleSolution] = None
  
  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("Send Image", precondition = () => checkLoggedIn.flatMap(_ => checkPuzzleActive), handler = msg => {
      logger.trace("Send Image")
      val findPortalReply = Await.result((puzzleService.get ? DrawPuzzleQuestion).mapTo[FindPortalGameReply], timeout.duration)
      findPortalReply match {
        case question @ PuzzleQuestion(puzzleIndex, puzzleState, image) => 
          activeQuestion = Some(question)
          val sendPhoto = PuzzleScenario.photoRequest(image, msg.chat.id)
          msgWithKeyboard = Await.result(env.escritoire.withReply(sendPhoto), timeout.duration)
          NextStage
        case NoOpenPuzzle => 
          QuitWithMessage(i18n"PuzzleScenario.alreadySolved")
        case NotInitialized => 
          QuitWithMessage(i18n"PuzzleScenario.notInitialized")
      }
    }),
    MessageOrCallbackStage("Skip Images / Solve Puzzle", precondition = () => checkLoggedIn.flatMap(_ => checkPuzzleActive), 
    cbHandler = cb => { // Next / Cancel
      implicit val esc = env.escritoire
      cb.data match {
        case Some("next") =>
          val nextFindPortalReply = Await.result((puzzleService.get ? DrawPuzzleQuestion).mapTo[FindPortalGameReply], timeout.duration)
          nextFindPortalReply match {
            case question @ PuzzleQuestion(puzzleIndex, puzzleState, image) => 
              msgWithKeyboard = msgWithKeyboard.replaceContent(PhotoContent(image, Some(i18n"PuzzleScenario.knowthisportal")))
              activeQuestion.foreach{q => puzzleService.get ! Unlock(q.puzzleIndex)}
              activeQuestion = Some(question)
              KeepStage
            case NoOpenPuzzle => 
              msgWithKeyboard.removeKeyboard
              activeQuestion.foreach{q => puzzleService.get ! Unlock(q.puzzleIndex)}
              activeQuestion = None
              QuitWithMessage(i18n"PuzzleScenario.alreadySolved")
            case NotInitialized => 
              msgWithKeyboard.removeKeyboard
              activeQuestion.foreach{q => puzzleService.get ! Unlock(q.puzzleIndex)}
              activeQuestion = None
              QuitWithMessage(i18n"PuzzleScenario.notInitialized")
          }
        case Some("cancel") =>
          msgWithKeyboard.removeKeyboard
          activeQuestion.foreach{q => puzzleService.get ! Unlock(q.puzzleIndex)}
          activeQuestion = None
          QuitWithMessage(i18n"CommonDialog.canceled")
        case _ =>
          msgWithKeyboard.removeKeyboard
          activeQuestion.foreach{q => puzzleService.get ! Unlock(q.puzzleIndex)}
          activeQuestion = None
          QuitWithError(i18n"CommonDialog.wrongInput")
      }
    }, msgHandler = msg => { // Solve
      val questionSolution = for{
        question    <- activeQuestion.toRight(i18n"PuzzleScenario.noPortalChosen")
        msgtext     <- msg.text.toRight(i18n"PuzzleScenario.noSolutionChosen")
        solution    <- PuzzleSolution.parseFromString(msgtext)
      } yield (question, solution)
      questionSolution match {
        case Right((question, solution)) => 
          val replyKeyboard = ReplyKeyboardMarkup.singleRow(Seq(
            KeyboardButton(i18n"CommonDialog.ok"), KeyboardButton(i18n"CommonDialog.cancel")
          ), resizeKeyboard = Some(true), oneTimeKeyboard = Some(true))
          env.escritoire(i18n"PuzzleScenario.reallyInsert/${solution.portalName},${question.puzzleIndex.display}",
            keyboard = Some(replyKeyboard))
          chosenQuestion = Some(question)
          chosenSolution = Some(solution)
          NextStage
        case Left(errMsg) => 
          implicit val esc = env.escritoire
          msgWithKeyboard.removeKeyboard
          activeQuestion.foreach{q => puzzleService.get ! Unlock(q.puzzleIndex)}
          activeQuestion = None
          QuitWithError(errMsg)
      }
    }),
    MessageStage("Submit Solution", precondition = () => checkLoggedIn.flatMap(_ => checkPuzzleActive), handler = msg => {
      logger.trace(s"Submit Solution: $msg")
      val nextStage: Either[String, ScenarioMessage] = for {
        text       <- msg.text.toRight(i18n"CommonDialog.canceled")
        _          <- Either.cond(text == i18n"CommonDialog.ok", (), i18n"CommonDialog.canceled")
        question   <- chosenQuestion.toRight(i18n"PuzzleScenario.noPortalChosen")
        solution   <- chosenSolution.toRight(i18n"PuzzleScenario.noSolutionChosen")
        solver     <- state.agentName.toRight(i18n"CommonDialog.notLoggedId")
        _          <- submitSafe(PuzzleEntry(solution.portalName, solver, solution.portalLink), question.puzzleIndex)
      } yield NamedStage("Skip Images / Solve Puzzle")

      nextStage match {
        case Left(errMsg) => 
          chosenQuestion.map(_.puzzleIndex).foreach (puzzleService.get ! Unlock(_))
          chosenQuestion = None
          QuitWithError(errMsg, removeKeyboard = true)
        case Right(nextStage) => 
          implicit val esc = env.escritoire
          env.escritoire("Fertig", Some(ReplyKeyboardRemove()))
          msgWithKeyboard.removeKeyboard
          val findPortalReply = Await.result((puzzleService.get ? DrawPuzzleQuestion).mapTo[FindPortalGameReply], timeout.duration)
          findPortalReply match {
            case question @ PuzzleQuestion(puzzleIndex, puzzleState, image) => 
              activeQuestion = Some(question)
              val sendPhoto = PuzzleScenario.photoRequest(image, msg.chat.id)
              msgWithKeyboard = Await.result(env.escritoire.withReply(sendPhoto), timeout.duration)
              nextStage
            case NoOpenPuzzle => 
              QuitWithMessage(i18n"PuzzleScenario.alreadySolved")
            case NotInitialized => 
              QuitWithMessage(i18n"PuzzleScenario.notInitialized")
          }
      }
    })
  )

  private def submitSafe(puzzleEntry: PuzzleEntry, puzzleIndex: PuzzleIndex): Either[String, Unit] = {
    val range = PuzzleRange(puzzleIndex)
    Try{ Await.result(SheetsReader.read(range), timeout.duration) }.toEither match {
      case Left(error) => 
        error.printStackTrace
        logger.error("Error reading existing entry", error)
        Left(i18n"CommonDialog.networkError")
      case Right(Some(_)) => 
        logger.debug(s"Try to solve already solved puzzle $puzzleEntry at $puzzleIndex")
        puzzleService.get ! SolvePuzzle(puzzleIndex)
        Left(i18n"PuzzleScenario.tooSlow")
      case Right(None) => 
        Try{ Await.result(puzzleEntry.writeTo(range), timeout.duration) }.toEither match {
          case Left(error) => 
            error.printStackTrace
            logger.error(s"Error writing puzzle entry $puzzleEntry to $puzzleIndex", error)
            Left(i18n"CommonDialog.networkError")
          case Right(writeResult) => 
            puzzleService.get ! SolvePuzzle(puzzleIndex)
            Right()
        }
    }
  }

  private def checkLoggedIn: Either[String, Unit] = Either.cond(state.agentName.isDefined, (), i18n"CommonDialog.notLoggedId")
  private def checkPuzzleActive: Either[String, Unit] = Either.cond(puzzleService.isDefined, (), i18n"PuzzleScenario.notEnabled")

}

object PuzzleScenario {
  def props = Props(new PuzzleScenario)

  def markup(implicit scenario: Scenario) = InlineKeyboardMarkup(
    Seq(
      Seq(InlineKeyboardButton.callbackData(i18n"PuzzleScenario.nonext", "next"),
      InlineKeyboardButton.switchInlineQueryCurrentChat(i18n"PuzzleScenario.sureiknow", "")),
      Seq(InlineKeyboardButton.callbackData(i18n"CommonDialog.cancel", "cancel"))
    )
  )

  def photoRequest(image: InputFile, recipient: Long)(implicit scenario: Scenario): Request[Message] = RequestBuilder()
    .withRecipient(recipient)
    .withReplyMarkup(PuzzleScenario.markup)
    .withContent(knowThisPortal(image))
    .build

  def knowThisPortal(image: InputFile)(implicit scenario: Scenario) = 
    PhotoContent(image, Some(i18n"PuzzleScenario.knowthisportal"))
}

case class PuzzleSolution(portalName: String, portalLink: String)
object PuzzleSolution {
    def parseFromString(inlineReply: String)(implicit scenario: Scenario): Either[String, PuzzleSolution] = {
        for {
            lines <- Either.cond(inlineReply.count(_ == '\n') == 1, inlineReply.split('\n'), i18n"PuzzleScenario.notAnAnswer")
            portalname <- Either.cond(!lines(0).isBlank, lines(0), i18n"PuzzleScenario.portalNameEmpty")
            intellink <- Either.cond(lines(1).contains("intel.ingress.com"), lines(1), i18n"PuzzleScenario.noLinkToIntel")
        } yield PuzzleSolution(portalname, intellink)
    }
}