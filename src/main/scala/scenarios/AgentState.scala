package scenarios

import com.osinka.i18n.Lang

case class AgentState(agentName: Option[String],
                      cellsState: Submitted,
                      selfieUploaded: Boolean,
                      lang: Lang)
case class Submitted(start: Boolean, end: Boolean)