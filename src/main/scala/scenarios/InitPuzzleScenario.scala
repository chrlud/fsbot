package scenarios

import actors.FindPortalGame
import actors.ServiceRegistry
import akka.actor.ActorRef
import akka.actor.Props
import scenarios.Scenario.QuitWithMessage
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider

class InitPuzzleScenario extends Scenario {
  lazy val puzzleService = ServiceRegistry.lookup[ActorRef]("PuzzleGame")

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("", precondition = checkPuzzleActive _, handler = _ => {
      puzzleService.get ! FindPortalGame.Init
      QuitWithMessage("Puzzle wird initialisiert...")
    })
  )

  private def checkPuzzleActive: Either[String, Unit] = Either.cond(puzzleService.isDefined, (), i18n"PuzzleScenario.notEnabled")
}

object InitPuzzleScenario {
  def props = Props(new InitPuzzleScenario)
}