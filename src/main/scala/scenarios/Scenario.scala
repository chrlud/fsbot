package scenarios

import akka.actor.Actor
import com.bot4s.telegram.methods.AnswerCallbackQuery
import com.bot4s.telegram.models.CallbackQuery
import com.bot4s.telegram.models.Message
import com.bot4s.telegram.models.ReplyKeyboardRemove
import scenarios.Scenario.ScenarioMessage
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider
import slogging.LazyLogging

import scala.util.Failure
import scala.util.Success
import scala.util.Try

trait Scenario extends Actor with LazyLogging {

  implicit val scenario = this

  import Scenario._

  var state: AgentState = _
  var env: actors.Environment = _

  val stages: Seq[ScenarioStage]
  var nActiveStage = 0

  def activeStage = stages(nActiveStage)

  override def receive: Receive = {
    case newState: AgentState =>
      logger.trace(s"Set new state to: $newState")
      state = newState
    case newEnvironment: actors.Environment =>
      logger.trace(s"Set new environment to: $newEnvironment")
      env = newEnvironment
    case msg: Message => 
      val result = for {
        _ <- assertState
        _ <- assertEnvironment
        _ <- assertMessageStage(activeStage)
        _ <- assertPrecondition(activeStage)
        handlerResult <- sendUpstreamToHandler(msg, activeStage)
      } yield handlerResult
      result match {
        case Right(scenarioMsg: ScenarioMessage) => sendDownstreamMessage(scenarioMsg)
        case Left(msg: String) => sendDownstreamError(msg)
      }
    case cb: CallbackQuery => 
      val result = for {
        _ <- assertState
        _ <- assertEnvironment
        _ <- assertCallbackStage(activeStage)
        _ <- assertPrecondition(activeStage)
        handlerResult <- sendUpstreamToHandler(cb, activeStage)
        _ <- Right(env.escritoire(AnswerCallbackQuery(cb.id)))
      } yield handlerResult
      result match {
        case Right(scenarioMsg: ScenarioMessage) => sendDownstreamMessage(scenarioMsg)
        case Left(msg: String) => sendDownstreamError(msg)
      }
  }

  private def assertState = Either.cond(null != state, (), "No State")//Try{ if(null == state) throw new Exception("No State") }
  private def assertEnvironment = Either.cond(null != env, (), "No Environment")//Try{ if(null == env) throw new Exception("No Environment")}
  private def assertMessageStage(stage: ScenarioStage) = stage match {
    case msgStage: MessageStage => Right(msgStage)
    case cbStage: CallbackStage => Left("")
    case msgCbStage: MessageOrCallbackStage => Right(msgCbStage)
  }
  private def assertCallbackStage(stage: ScenarioStage) = stage match {
    case msgStage: MessageStage => Left("")
    case cbStage: CallbackStage => Right(cbStage)
    case msgCbStage: MessageOrCallbackStage => Right(msgCbStage)
  }
  private def assertPrecondition(stage: ScenarioStage) = (stage match {
    case MessageStage(_, _, precondition) => precondition
    case CallbackStage(_, _, precondition) => precondition
    case MessageOrCallbackStage(_, _, _, precondition) => precondition
  }).apply
  private def sendUpstreamToHandler(msg: Message, stage: ScenarioStage) = {
    val handler = stage match {
      case MessageStage(_, handler, _) => Right(handler)
      case _: CallbackStage => Left("")
      case MessageOrCallbackStage(_, msgHandler, _, _) => Right(msgHandler)
    }
    Try{
    handler.map{handler => 
      logger.trace(s"Apply to ScenarioStage: ${stage.name} with message: $msg")
      handler.apply(msg)
    }} match {
      case Success(result) => 
        logger.trace(s"Result of Scenario: $result")
        result
      case Failure(exception) => 
        logger.error(s"Exception during Scenario", exception)
        Left(s"Exception during Scenario")
    }
}
  private def sendUpstreamToHandler(cb: CallbackQuery, stage: ScenarioStage) = (stage match {
    case _: MessageStage => Left("")
    case CallbackStage(_, handler, _) => Right(handler)
    case MessageOrCallbackStage(_, _, cbHandler, _) => Right(cbHandler)
  }).map(_.apply(cb))
  private def sendDownstreamMessage(scenarioMessage: ScenarioMessage) = scenarioMessage match {
    case KeepStage => 
      logger.debug("KeepStage")
    case NextStage => 
      logger.trace("NextStage")
      nActiveStage += 1
    case NextStage(msg) =>
      logger.trace(s"NextStage($msg)")
      env.escritoire(msg)
      nActiveStage += 1
    case NamedStage(name) => 
      Try{stages.indexWhere(stage => stage.name == name)} match {
        case Success(ix) => 
          logger.trace(s"NamedStage($name) => ix = $ix")
          nActiveStage = ix
        case Failure(exception) => 
          logger.error(s"NamedStage($name) => No Stage found!", exception)
          resetStageAndQuit
      }
    case QuitScenario(result) => 
      logger.trace("Quit")
      resetStageAndQuitWithState(result)
    case QuitWithMessage(msg, removeKeyboard) => 
      logger.trace(s"Quit with Message: $msg (removeKeyboard: $removeKeyboard)")
      if(removeKeyboard)
        env.escritoire(msg, Some(ReplyKeyboardRemove()))
      else
        env.escritoire(msg)
      resetStageAndQuit
    case QuitWithError(msg, removeKeyboard) => 
      logger.error(s"Quit with Error: $msg")
      sendDownstreamError(msg)
  }
  private def sendDownstreamError(msg: String, removeKeyboard: Boolean = false) = {
    logger.error(s"Scenario failed with message '$msg'")
    val errMsg = i18n"CommonDialog.errorWithReason/${msg}"
    if(removeKeyboard)
        env.escritoire(errMsg, Some(ReplyKeyboardRemove()))
      else
        env.escritoire(errMsg)
    resetStageAndQuit
  }

  private def resetStageAndQuit = {
    this.nActiveStage = 0
    context.parent ! QuitScenario(state)
  }
  private def resetStageAndQuitWithState(s: AgentState) = {
    this.nActiveStage = 0
    context.parent ! QuitScenario(s)
  }

}

object Scenario {

  trait ScenarioMessage

  final case object NextStage extends ScenarioMessage

  final case class NextStage(msg: String) extends ScenarioMessage

  final case object KeepStage extends ScenarioMessage

  final case class NamedStage(name: String) extends ScenarioMessage

  final case class QuitScenario(result: AgentState) extends ScenarioMessage // better Done[T]?

  final case class QuitWithError(msg: String, removeKeyboard: Boolean = false) extends ScenarioMessage

  final case class QuitWithMessage(msg: String, removeKeyboard: Boolean = false) extends ScenarioMessage

}

sealed trait ScenarioStage{
  val name: String
}
case class MessageStage(
  override val name: String, 
  handler: Message => ScenarioMessage, 
  precondition: () => Either[String, Unit] = () => Right()) extends ScenarioStage
case class CallbackStage(
  override val name: String, 
  handler: CallbackQuery => ScenarioMessage, 
  precondition: () => Either[String, Unit] = () => Right()) extends ScenarioStage
case class MessageOrCallbackStage(
  override val name: String, 
  msgHandler: Message => ScenarioMessage, 
  cbHandler: CallbackQuery => ScenarioMessage, 
  precondition: () => Either[String, Unit] = () => Right()) extends ScenarioStage