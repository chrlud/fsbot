package scenarios.inlkb

import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.util.Try

import akka.util.Timeout
import com.bot4s.telegram.models.InlineKeyboardMarkup
import com.bot4s.telegram.models.Message
import telegram.Escritoire
import telegram.RequestBuilder
import telegram.RequestContent
import com.bot4s.telegram.methods.DeleteMessage
import scala.util.Success
import scala.util.Failure
import com.bot4s.telegram.methods.EditMessageText
import com.bot4s.telegram.methods.EditMessageReplyMarkup
import com.bot4s.telegram.methods.AnswerCallbackQuery

object InlineKeyboardOps {
    implicit class MessageWithInlKB(msg: Message)(implicit esc: Escritoire, ec: ExecutionContext, to: Timeout){
        def replaceContent(content: RequestContent): Message = {
            val request = RequestBuilder()
                .withRecipient(msg.chat.id)
                .withContent(content)
                .withReplyMarkup(msg.replyMarkup.get)
                .build
            Try{ Await.result(esc.withReply(request), to.duration) } match {
                case Success(newMessage) => 
                    esc.apply(DeleteMessage(msg.chat.id, msg.messageId))
                    newMessage
                case Failure(exception) => msg
            }
        }
        def updateMessage(txt: String, newMarkup: Option[InlineKeyboardMarkup]): Message = {
            esc.apply(EditMessageText(Some(msg.chat.id), Some(msg.messageId), text = txt, replyMarkup = newMarkup))
            msg
        }
        def updateText(txt: String): Message = updateMessage(txt, msg.replyMarkup)
        def updateMarkup(markup: InlineKeyboardMarkup): Message = {
            esc.apply(EditMessageReplyMarkup(Some(msg.chat.id), Some(msg.messageId), replyMarkup = Some(markup)))
            msg
        }
        def removeKeyboard: Message = {
            esc.apply(EditMessageReplyMarkup(Some(msg.chat.id), Some(msg.messageId), replyMarkup = None))
            msg
        }
    }
  
}
