package scenarios.admin

import scenarios.Scenario
import slogging.LazyLogging
import scenarios.ScenarioStage
import akka.actor.Props
import config.ConfigService
import scenarios.MessageStage
import Scenario.{QuitScenario, NextStage, QuitWithMessage}
import com.bot4s.telegram.models.InlineKeyboardMarkup
import com.bot4s.telegram.models.InlineKeyboardButton
import com.bot4s.telegram.models.Message
import telegram.RequestBuilder
import telegram.TextContent
import config.TimeoutConfig._
import scala.concurrent.Await
import scenarios.CallbackStage
import actors.ServiceRegistry
import akka.actor.ActorRef
import actors.FindPortalGame
import actors.BotActor
import scala.io.Source
import actors.BotInterface
import main.FsMain
import actors.SplitUsers
import akka.pattern.ask
import scenarios.Commands
import scenarios.UserInfo
import scala.concurrent.Future
import scenarios.Emoticon

class AdminScenario extends Scenario with LazyLogging {
    implicit val dispatcher = context.dispatcher

    val adminId = ConfigService.adminTgId.get
    val restartCommand = ConfigService.resetCommand.get

    var messageWithKeyboard: Message = _

    lazy val puzzleService = ServiceRegistry.lookup[ActorRef]("PuzzleGame")
    lazy val botActor = ServiceRegistry.lookup[ActorRef]("BotActor").get
    lazy val bot = ServiceRegistry.lookup[BotInterface]("BotInterface").get
    lazy val splitUsers = ServiceRegistry.lookup[ActorRef]("SplitUsers").get

    private def queryUserInfo(accountActor: ActorRef): Future[UserInfo] = 
        (accountActor ? Commands.GetUserInfo).mapTo[UserInfo]
    private def queryUserInfos(accountActors: List[ActorRef]): Future[List[UserInfo]] = 
        Future.sequence(accountActors.map(queryUserInfo(_)))
    private def queryUserInfos(): Future[List[UserInfo]] =
        (splitUsers ? SplitUsers.Accounts).mapTo[Map[Long, ActorRef]]
            .flatMap{accountMap => queryUserInfos(accountMap.values.toList)}
    private def queryFlushingState(): Future[BotActor.FlushingState] =
        (botActor ? BotActor.IsFlushing).mapTo[BotActor.FlushingState]

    override val stages: Seq[ScenarioStage] = Seq(
        MessageStage("selection", msg => {
            val userInfos = queryUserInfos()
            val flushing = queryFlushingState()
            val msgReply = userInfos.zip(flushing).map{case (userInfos, flushing) => 
                AdminScenario.selectionMessage(msg.chat.id, userInfos, flushing)
            }.flatMap{request => 
                env.escritoire.withReply(request)
            }
            messageWithKeyboard = Await.result(msgReply, timeout.duration)
            NextStage
        }),
        CallbackStage("execute", cb => {
            cb.data match {
                case Some("cancel") =>
                    QuitScenario(state)
                case Some("initpuzzle") => 
                    puzzleService.get ! FindPortalGame.Init
                    QuitWithMessage("Puzzle wird initialisiert...")
                case Some("flushing") =>
                    botActor ! BotActor.SwitchFlushing
                    QuitWithMessage("Switching Flushing...")
                case Some("userinfos") => 
                    val userInfos = queryUserInfos()

                    val userTable = userInfos.map{userInfos => 
                        val header = "|Tg|Agent|Start|End|Selfie|Lang|"
                        val body = userInfos.map{userInfo => 
                            List(s"@${userInfo.tgName}", userInfo.agentName, Emoticon.CheckOrCross(userInfo.start), 
                            Emoticon.CheckOrCross(userInfo.end), Emoticon.CheckOrCross(userInfo.selfie), userInfo.lang).mkString("|", "|", "|")
                        }.mkString("\n")
                        s"```\n$header\n$body\n```"
                    }
                    
                    val request = userTable.map{msgText =>
                        RequestBuilder()
                            .withRecipient(cb.from.id)
                            .withTextContent(msgText)
                            .build
                    }
                    env.escritoire(Await.result(request, timeout.duration))
                    QuitScenario(state)
                case Some("ip") =>
                    val ipString = Source.fromURL("http://whatismyip.akamai.com/").mkString
                    QuitWithMessage(s"Server IP is:\n $ipString")
                case Some("hreset") =>
                    import sys.process._
                    import scala.language.postfixOps
                    val result = (s"at now + 1 minute -f $restartCommand" !!)
                    logger.info(s"Restart Scheduled:\n$result")

                    bot.shutdown
                    QuitWithMessage("Restarting...")
                case Some("shutdown") => 
                    FsMain.shutdown
                    QuitWithMessage("Shutting down...")
                case _ => QuitScenario(state)
            }
            
        })
    )

  
}

object AdminScenario {
    def props = Props(new AdminScenario())

    def selectionKeyboard = InlineKeyboardMarkup(
        Seq(
            Seq(InlineKeyboardButton.callbackData("Init Puzzle", "initpuzzle"), InlineKeyboardButton.callbackData("Switch Flushing", "flushing")),
            Seq(InlineKeyboardButton.callbackData("User Infos", "userinfos")),
            Seq(InlineKeyboardButton.callbackData("Server IP", "ip"), InlineKeyboardButton.callbackData("Hard Reset", "hreset")),
            Seq(InlineKeyboardButton.callbackData("Shutdown", "shutdown")),
            Seq(InlineKeyboardButton.callbackData("Abbruch", "cancel"))
        )
    )

    def selectionMessage(recipient: Long, userInfos: List[UserInfo], flushingState: BotActor.FlushingState) = RequestBuilder()
        .withRecipient(recipient)
        .withContent(TextContent(s"""|__ *Admin Menu* __
            |*Flushing*: ${flushingState.isFlushing}
            |*Users*: ${userInfos.map(_.tgName).mkString(", ")}
            |*Registered Users*: ${userInfos.filter(_.start).map(_.agentName).mkString(", ")}""".stripMargin('|')))
        .withReplyMarkup(selectionKeyboard)
        .build
}