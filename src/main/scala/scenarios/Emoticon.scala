package scenarios

object Emoticon {
  val DE_FLAG: String =
    (Character.toChars(127465) ++ Character.toChars(127466)).mkString
  val US_FLAG: String =
    (Character.toChars(127482) ++ Character.toChars(127480)).mkString
  val GREEN_CHECK: String = Character.toChars(9989).mkString
  val RED_CROSS: String = "❌"

  def CheckOrCross(b: Boolean) = if(b) GREEN_CHECK else RED_CROSS
}
