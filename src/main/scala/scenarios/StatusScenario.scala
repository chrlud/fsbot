package scenarios

import actors.FindPortalGame
import actors.FindPortalGame.NotInitialized
import actors.FindPortalGame.PuzzleProgress
import actors.FindPortalGame.QueryProgress
import actors.ServiceRegistry
import akka.actor.ActorRef
import akka.actor.Props
import akka.pattern.ask
import config.ConfigService
import config.EventConfig
import config.EventConfig.ReferencePoint._
import config.EventState._
import config.TimeoutConfig._
import scenarios.Scenario.QuitScenario
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider

import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future

class StatusScenario extends Scenario {
  implicit val ec: ExecutionContext = context.system.dispatchers.lookup("blocking-io-dispatcher")

  def presentEmoji(b: Boolean) = if(b) "✓" else "-"

  def progress(eventConfig: EventConfig): String = {
    val locale = state.lang.locale
    if(eventConfig.isUpcoming) i18n"StatusScenario.startin/${eventConfig.durationToFmt(INPROGRESS, locale)}"
    else if(eventConfig.isInprogress) i18n"StatusScenario.running/${eventConfig.durationToFmt(INPROGRESS, locale, RIGHT)}"
    else i18n"StatusScenario.eventended"
  }
  def hints(eventConfig: EventConfig, submitted: Submitted): String = {
    val locale = state.lang.locale
    if(!submitted.start && eventConfig.canUploadStart(submitted)) i18n"StatusScenario.sendstartnow"
    else if(eventConfig.isUpcoming) i18n"StatusScenario.acceptstart/${eventConfig.durationToFmt(START, locale)}"
    else if(!submitted.start && !eventConfig.canUploadStart(submitted)) i18n"StatusScenario.startphaseclosed"
    else if(!submitted.end && eventConfig.canUploadEnd(submitted)) i18n"StatusScenario.sendendnow"
    else if(!submitted.end && !eventConfig.canUploadEnd(submitted)) i18n"StatusScenario.endphaseclosed"
    else i18n"StatusScenario.endsubmitted"
  }
  def loggedIn(userState: AgentState): String =
    Seq(
      if(userState.agentName.isDefined) i18n"StatusScenario.loggedinas/${userState.agentName.get}"
      else i18n"StatusScenario.notloggedin",
      i18n"StatusScenario.startvalues/${presentEmoji(userState.cellsState.start)}",
      i18n"StatusScenario.endvalues/${presentEmoji(userState.cellsState.`end`)}",
      i18n"StatusScenario.selfie/${presentEmoji(userState.selfieUploaded)}",
    ).mkString("\n")

  def puzzle: String = {
    val reply = for{
      puzzleService <- fut(ServiceRegistry.lookup[ActorRef]("PuzzleGame"))  // TODO: Schau mal wie das mit CATS besser geht
      puzzleReply <- (puzzleService ? QueryProgress).mapTo[FindPortalGame.FindPortalGameReply]
    } yield puzzleReply
    val message = reply.map{
      case NotInitialized => i18n"StatusScenario.puzzlenotinitialized"
      case PuzzleProgress(n, solved, inprogress) => i18n"StatusScenario.puzzleProgress/${solved * 100 / n},${n},${solved},${inprogress})"
    }.recover{
      case _ => i18n"PuzzleScenario.notEnabled"
    }

    Await.result(message, timeout.duration)
  }

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage("Send status", _ => {
      val eventConfig = ConfigService.eventConfig.get

      val progressMsg = progress(eventConfig)
      val loggedInMsg = loggedIn(state)
      val hintsMsg: String = hints(eventConfig, state.cellsState)
      val puzzleMsg = puzzle
      env.escritoire(s"$progressMsg\n\n$loggedInMsg\n\n$hintsMsg\n\n$puzzleMsg")

      QuitScenario(state)
    })
  )

  private def fut[T](opt: Option[T]): Future[T] = opt match {
    case Some(value) => Future.successful(value)
    case None => Future.failed(new Exception("Value not present"))
  }
}

object StatusScenario {
  def props = Props(new StatusScenario)
}