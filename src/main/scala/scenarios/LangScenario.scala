package scenarios

import akka.actor.Props
import com.bot4s.telegram.models.KeyboardButton
import com.bot4s.telegram.models.ReplyKeyboardMarkup
import com.osinka.i18n.Lang
import com.osinka.i18n.Messages
import scenarios.i18n.I18NStringContext._
import scenarios.i18n.LocaleProviderInstances.scenarioLocaleProvider

class LangScenario extends Scenario {
  import Scenario._
  import LangScenario._

  override val stages: Seq[ScenarioStage] = Seq(
    MessageStage(
      "Change Language",
      _ => {
        val lmsg: String = Seq(
          i18n"LangScenario.actualLang/${display(state.lang)}",
          i18n"LangScenario.chooseLang",
        ).mkString("\n")
        env.escritoire(lmsg, keyboard = Some(keyboard))
        NextStage
      }
    ),
    MessageStage(
      "Chosen Language",
      msg => {
        (msg.text match {
          case Some(Emoticon.DE_FLAG) => Some(Lang("de"))
          case Some(Emoticon.US_FLAG) => Some(Lang("en"))
          case _ => None
        }) match {
          case Some(newLang) =>
            val newState = state.copy(lang = newLang)
            env.escritoire(Messages("LangScenario.newLang", display(newLang))(newLang))
            QuitScenario(newState)
          case None => 
            QuitWithMessage(i18n"CommonDialog.canceled")
        }
      }
    )
  )

}

object LangScenario {
  def props = Props(new LangScenario)

  val keyboard = ReplyKeyboardMarkup(
    Seq(
      Seq(KeyboardButton(Emoticon.DE_FLAG), KeyboardButton(Emoticon.US_FLAG)),
      Seq(KeyboardButton("Abbruch"))
    ),
    oneTimeKeyboard = Some(true),
    resizeKeyboard = Some(true)
  )

  def display(lang: Lang): String = lang.language match {
    case "de" => "Deutsch"
    case "en" => "English"
    case _    => "Deutsch"
  }
}
