package scenarios

import akka.actor.{Actor, ActorRef}
import com.bot4s.telegram.models.Message
import slogging.LazyLogging

import scala.collection.mutable
import com.bot4s.telegram.models.CallbackQuery
import com.osinka.i18n.Messages
import actors.Environment

trait Commands extends Actor with LazyLogging {

  import Scenario.QuitScenario

  var activeScenario: Option[ActorRef] = None
  var state: AgentState = _
  var env: Environment = _

  val registeredScenarios: mutable.Map[String, ActorRef] = mutable.Map()
  def registerCommandWith(cmdName: String, scenario: ActorRef): Unit = {
    registeredScenarios += cmdName -> scenario
  }

  override def receive: Receive = {
    case QuitScenario(finalState) =>
      logger.trace(s"Scenario $activeScenario finished with state $finalState.")
      activeScenario = None
      this.state = finalState
      registeredScenarios.foreach(_._2 ! state)
    case msg: Message =>
      logger.trace(s"Received Message $msg")
      activeScenario match {
      case Some(scenario) =>
        logger.trace(s"Dispatch Message to Scenario ($scenario), MessageId: ${msg.messageId}")
        scenario ! msg
      case None =>
        val lookup = for {
          txt <- msg.text
          (name, params) <- extractCommand(txt)
          scenario <- registeredScenarios.get(name)
        } yield (scenario, params)
        lookup match {
          case Some((scenario, _)) =>
            logger.trace(s"Scenario $scenario started: , MessageId: ${msg.messageId}")
            activeScenario = Some(scenario)
            logger.trace(s"Dispatch Message to Scenario ($scenario), MessageId: ${msg.messageId}")
            scenario ! msg
          case None =>
            val msg = Messages("CommonDialog.noCommand")(state.lang)
            env.escritoire(msg)
        }
      }
    case cb: CallbackQuery =>
      activeScenario.foreach(_ ! cb)
    case Commands.GetUserInfo => 
      sender ! UserInfo(env.user.username.getOrElse("NoTgId"), state.agentName.getOrElse("NoAgentName"), state.cellsState.start, state.cellsState.end, state.selfieUploaded, state.lang.language)
  }

  private[this] def extractCommand(txt: String): Option[(String, Seq[String])] = {
    if (!txt.startsWith("/")) {
      None
    } else {
      val commandAndParams = txt.drop(1).split(' ')
      val cmd = commandAndParams(0)
      val params = commandAndParams.drop(1)
      Some(cmd, params)
    }
  }
}

object Commands {
  case object GetUserInfo
}

case class UserInfo(tgName: String, agentName: String, start: Boolean, end: Boolean, selfie: Boolean, lang: String)