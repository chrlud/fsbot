package config

import java.util.Locale

import com.github.nscala_time.time.Imports._
import com.typesafe.config.Config
import org.joda.time.{ReadableDuration, ReadableInstant, Seconds}
import org.joda.time.format.PeriodFormat
import scenarios.Submitted

final object EventState extends Enumeration {
  type EventState = Value
  val START, END, INPROGRESS, UPCOMING, CLOSED = Value
}

case class EventConfig(
    EVENT_NAME: String,
    EVENT_CHANNEL: String,
                      event_interval: Interval,
                      startstats_interval: Interval,
                      endstats_interval: Interval,
                      enforceStart: Boolean,
                      enfoceEnd: Boolean,
                      allowOverwriteEnd: Boolean,
                      puzzle_refresh_rate_seconds: Int
) {
  import EventState._
  import config.EventConfig.ReferencePoint._

  def fmt(locale: Locale) = PeriodFormat.wordBased(locale)
  val fmtDate = DateTimeFormat.forPattern("dd.MM.yyyy")
  val fmtTime = DateTimeFormat.forPattern("HH:mm")

  def isInprogress: Boolean = event_interval.containsNow
  def isUpcoming: Boolean = event_interval.isAfterNow
  def isClosed: Boolean = event_interval.isBeforeNow && endstats_interval.isBeforeNow
  def canUploadStart(submitted: Submitted): Boolean = 
    if(submitted.start) false
    else if(enforceStart) startstats_interval.containsNow
    else startstats_interval.getStart.isBeforeNow
  def canUploadEnd(submitted: Submitted): Boolean =
    if(!allowOverwriteEnd && submitted.end) false
    else if(enfoceEnd) endstats_interval.containsNow
    else startstats_interval.getStart.isBeforeNow
  
  def durationTo(eventState: EventState, ref: ReferencePoint = LEFT): Option[Duration] = {
    val now = DateTime.now
    (eventState, ref) match {
      case (UPCOMING, LEFT) => None
      case (UPCOMING, RIGHT) => durationTo(startstats_interval.getStart)
      case (CLOSED, LEFT) => durationTo(endstats_interval.getEnd)
      case (CLOSED, RIGHT) => None
      case (START, refPoint) => durationTo(startstats_interval, refPoint)
      case (INPROGRESS, refPoint) => durationTo(event_interval, refPoint)
      case (END, refPoint) => durationTo(endstats_interval, refPoint)
      case _ => None
    }
  }
  private def durationTo(interval: Interval, ref: ReferencePoint): Option[Duration] = {
    durationTo(ref match {
        case LEFT => interval.getStart
        case RIGHT => interval.getEnd
      })
  }
  private def durationTo(instant: ReadableInstant): Option[Duration] = {
    val now = DateTime.now
    if(now < instant) Some(Seconds.secondsBetween(now, instant).toStandardDuration)
    else None
  }
  def min(i1: ReadableInstant, i2: ReadableInstant) = if(i1 < i2) i1 else i2
  def durationToFmt(eventState: EventState, locale: Locale, ref: ReferencePoint = LEFT): String = {
    durationTo(eventState, ref).map{d =>
      fmt(locale).print(d.toPeriodFrom(DateTime.now))
    }.getOrElse("???")
  }
}


object EventConfig {
  def load(eventConfigPath: Config): EventConfig = {
    val fmt = DateTimeFormat.forPattern(eventConfigPath.getString("DATE_FORMAT"))
    val eventFrom = fmt.parseDateTime(eventConfigPath.getString("EVENT_START_DATE"))
    val eventTo = fmt.parseDateTime(eventConfigPath.getString("EVENT_END_DATE"))
    val startFrom = fmt.parseDateTime(eventConfigPath.getString("STARTSTATS_WINDOW_FROM"))
    val startTo = fmt.parseDateTime(eventConfigPath.getString("STARTSTATS_WINDOW_UNTIL"))
    val endFrom = fmt.parseDateTime(eventConfigPath.getString("ENDSTATS_WINDOW_FROM"))
    val endTo = fmt.parseDateTime(eventConfigPath.getString("ENDSTATS_WINDOW_UNTIL"))
    val enforceStart = eventConfigPath.getBoolean("ENFORCE_START_INTERVAL")
    val enforceEnd = eventConfigPath.getBoolean("ENFORCE_END_INTERVAL")
    val allowOverwriteEnd = eventConfigPath.getBoolean("ALLOW_OVERWRITE_ENDSTATS")
    val puzzleRefreshRate = eventConfigPath.getInt("PUZZLE_REFRESH_RATE_SECONDS")
    EventConfig(
      eventConfigPath.getString("EVENT_NAME"),
      eventConfigPath.getString("EVENT_CHANNEL"),
      new Interval(eventFrom, eventTo),
      new Interval(startFrom, startTo),
      new Interval(endFrom, endTo),
      enforceStart, enforceEnd, allowOverwriteEnd, puzzleRefreshRate
    )
  }

  final object ReferencePoint extends Enumeration {
    type ReferencePoint = Value
    val LEFT, RIGHT = Value
  }

}

case class UserConfig(telegramName: String, agentName: String)

case class RuntimeConfig(selfieDirectory: String,
                         portalLookupFile: String,
                         puzzleEnabled: Boolean,
                         puzzleDirectory: String)

case class ParserConfig(knownHeaders: List[String])

case class ScoresheetConfig(sheetsId: String, tgIdColumn: String, tgNameColumn: String)