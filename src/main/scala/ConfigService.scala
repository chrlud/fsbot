package config

import gsheets.RequestFactoryConfig

object ConfigService {

  var eventConfig: Option[EventConfig] = None
  var sheetsConfig: Option[RequestFactoryConfig] = None
  var scoresheetConfig: Option[ScoresheetConfig] = None

  var portalsucheSheetId: Option[String] = None
  var telegramBotId: Option[String] = None
  var delayBeforeStart: Option[Int] = None
  var adminTgId: Option[Long] = None
  var resetCommand: Option[String] = None

  var userConfigs: Set[UserConfig] = Set()
  var parserConfig: Option[ParserConfig] = None

  def userConfig(telegramName: String): Option[UserConfig] =
  userConfigs.find( _.telegramName == telegramName )

  var runtimeConfig: Option[RuntimeConfig] = None

}

object TimeoutConfig {
  import akka.util.Timeout
  import scala.concurrent.duration._
  implicit val timeout = Timeout(3 minutes)
}