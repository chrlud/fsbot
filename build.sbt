ThisBuild / version := "0.9.5-SNAPSHOT"
ThisBuild / scalaVersion := "2.12.14"
ThisBuild / organization := "de.digitalculture"

ThisBuild / assemblyMergeStrategy := {
  case PathList("META-INF", "versions", "9", "module-info.class") => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case PathList("META-INF", "DEPENDENCIES") => MergeStrategy.discard
  case PathList("META-INF", "LICENSE.txt") => MergeStrategy.discard
  case "reference.conf" => MergeStrategy.concat
  case _ => MergeStrategy.deduplicate
}

lazy val fsbot = (project in file("."))
  .settings(
    name := "FsBot",
    libraryDependencies ++= Seq(
      "com.typesafe" % "config" % "1.4.2",
      "com.bot4s" %% "telegram-core" % "5.6.0",
      "com.bot4s" %% "telegram-akka" % "5.6.0",
      "com.google.api-client" % "google-api-client" % "1.35.1",
      "com.google.oauth-client" % "google-oauth-client-jetty" % "1.34.1",
      "com.google.apis" % "google-api-services-sheets" % "v4-rev20220606-1.32.1",
      "com.google.code.gson" % "gson" % "2.9.0",
      "com.github.nscala-time" %% "nscala-time" % "2.30.0",
      "biz.enef" %% "slogging-slf4j" % "0.6.1",
      "com.osinka.i18n" %% "scala-i18n" % "1.0.3",
      "ch.qos.logback" % "logback-classic" % "1.2.11",
      "org.scalatest" %% "scalatest" % "3.2.3" % Test
    )
  )
